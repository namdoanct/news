<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PostController;
use App\Models\Comment;
use Encore\Admin\Grid\Displayers\Prefix;
use Illuminate\Support\Facades\Route;

use PhpParser\Node\Name;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/



Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'check_login_admin'], function () {

// Route::group(['middleware' => ['check_login_admin'], 'as' => 'admin.'], function () {

    // Route::group(['prefix' => 'admin.',  'middleware' => 'CheckAdminLogin'], function(){


    Route::get('login', [AuthController::class, 'getLogin'])->name('login');
    Route::post('login', [AuthController::class, 'postLogin'])->name('login.handle');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');


    Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('index');
        Route::get('/create', [CategoryController::class, 'create'])->name('create');
        Route::post('/store', [CategoryController::class, 'store'])->name('store');
        Route::get('/{id}/show', [CategoryController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('edit');
        Route::put('/{id}/update', [CategoryController::class, 'update'])->name('update');
        Route::delete('/{id}/delete', [CategoryController::class, 'destroy'])->name('destroy');
    });

    Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {
        Route::get('/', [PostController::class, 'index'])->name('index');
        Route::get('/create', [PostController::class, 'create'])->name('create');
        Route::post('/store', [PostController::class, 'store'])->name('store');
        Route::get('/{id}/show', [PostController::class, 'show'])->name('show');
        Route::get('/{id}/edit', [PostController::class, 'edit'])->name('edit');
        Route::put('/{id}/update', [PostController::class, 'update'])->name('update');
        Route::delete('/{id}/delete', [PostController::class, 'destroy'])->name('destroy');
        Route::post('/{id}/save-feature', [PostController::class, 'saveFeature'])->name('save-feature');
    });

    Route::group(['prefix' => 'comments', 'as' => 'comments.'], function () {
        Route::get('/', [Comment::class, 'index'])->name('index');
    });
});








// Route::group(['middleware' => ['check_login_admin'] , 'as' => 'admin.'], function () {

//     Route::resource('category','CategoryController');

// 	// Admin Dashboard
// 	Route::get('/', [DashboardController::class, 'index'])->name('dashboard');	

//     // route for module Category
// 	Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
//         Route::get('/', [CategoryController::class, 'index'])->name('index');
//         Route::get('/create', [CategoryController::class, 'create'])->name('create');
//         Route::post('/store', [CategoryController::class, 'store'])->name('store');
//         Route::get('/{id}/show', [CategoryController::class, 'show'])->name('show');
//         Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('edit');
//         Route::put('/{id}/update', [CategoryController::class, 'update'])->name('update');
//         Route::delete('/{id}/delete', [CategoryController::class, 'destroy'])->name('destroy');
//     });

//     Route::group(['prefix' => 'posts', 'as' => 'posts.'], function() {
//         Route::get('/', [PostController::class, 'index'])->name('index');
//         Route::get('/create', [PostController::class, 'create'])->name('create');
//         Route::post('/store', [PostController::class, 'store'])->name('store');
//         Route::get('/{id}/show', [PostController::class, 'show'])->name('show');
//         Route::get('/{id}/edit', [PostController::class, 'edit'])->name('edit');
//         Route::put('/{id}/update', [PostController::class, 'update'])->name('update');
//     });

// });
