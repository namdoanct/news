<?php

use App\Http\Controllers\AjaxApiController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubscribePostController;
use App\Http\Controllers\UserQueueController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\TestSendMailController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/




require __DIR__ . '/admin.php';

require __DIR__ . '/auth.php';


Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/posts', [PostController::class, 'index'])->name('posts.index');

// Route::PostController::class->group(function () {
//     Route::get('/posts', 'show');
//     Route::post('/post', 'store');
// });

// Route::prefix('/posts')-> group(function() {
//     Route::get('/', [PostController::class, 'index'])->name('index');

// })-> name('posts.');


Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {
    Route::get('/', [PostController::class, 'index'])->name('index');
    // ---> URL : 127.0.0.1:8000/posts (method : GET)

    Route::get('/create', [PostController::class, 'create'])->name('create');
    // ---> URL : 127.0.0.1:8000/posts/create (method : GET)


    Route::post('/', [PostController::class, 'store'])->name('store');
    // ---> URL : 127.0.0.1:8000/posts (method: POST)


    Route::get('/{id}', [PostController::class, 'show'])->name('show');
    // ---> URL : 127.0.0.1:8000/posts/{post ID}   (method : GET)


    Route::get('/{id}/edit', [PostController::class, 'edit'])->name('edit');
    // ---> URL : 127.0.0.1:8000/posts/{ ID}/edit  (method: GET)

    Route::put('/{id}', [PostController::class, 'update'])->name('update');
    // ---> URL : 127.0.0.1:8000/posts/{ ID}  (method: PUT)


    Route::delete('/{id}', [PostController::class, 'destroy'])->name('destroy');
    // ---> URL : 127.0.0.1:8000/posts/{ ID}  (method : DELETE)
});

Route::get('/category/{id}/post', [CategoryController::class, 'index'])->name('category-list-post');


Route::get('/category', [CategoryController::class, 'index'])->name('category-list');


Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'handleLoginForm'])->name('login.handle');
Route::post('logout', [LoginController::class, 'logout'])->name('logout');

Route::get('profile', [ProfileController::class, 'showProfile'])->name('profile')->middleware('check_login_user');

Route::post('subscribe_post', [SubscribePostController::class, 'store'])->name('subscribe_post');

Route::get('test-send-mail', [TestSendMailController::class, 'send'])->name('test-send-mail');

Route::get('/ajax', [AjaxApiController::class, 'index'])->name('api.index');


Route::get('/test-email', [JobController::class, 'index'])->name('test-email');

// Route::get('test-email', 'JobController@processQueue');

Route::get('/file-import', [UserController::class, 'importView'])->name('import-view');
Route::post('/import', [UserController::class, 'import'])->name('import');
Route::get('/export-users', [UserController::class, 'exportUsers'])->name('export-users');

// Route::post('/import', UserController::class, 'importView')->name('import');


Route::get('/import-with-queue',[UserQueueController::class, 'import_with_queue'])->name('import-with-queue');
Route::post('/import-queue', [UserQueueController::class, 'import-queue'])->name('import-queue');