<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegistrationController;
use App\Http\Controllers\Api\CategoryProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProfileController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class, 'login'])->name('api.login');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
});


// Lấy danh sách sản phẩm
// Route::get('products', [ProductController::class, 'index'])->name('api.products.index');

// // Lấy thông tin sản phẩm theo id
// Route::get('products/{id}', 'Api\ProductController@show')->name('products.show');

// // Thêm sản phẩm mới
// Route::post('products', 'Api\ProductController@store')->name('products.store');

// // Cập nhật thông tin sản phẩm theo id
// # Sử dụng put nếu cập nhật toàn bộ các trường
// Route::put('products/{id}', 'Api\ProductController@update')->name('products.update');
// # Sử dụng patch nếu cập nhật 1 vài trường
// Route::patch('products/{id}', 'Api\ProductController@update')->name('products.update');

// // Xóa sản phẩm theo id
// Route::delete('products/{id}', 'Api\ProductController@destroy')->name('products.destroy');

// Route::get('sp', [ProductController::class, 'index']);
// Route::post('sp', [ProductController::class, 'store']);


// Lấy danh sách danh muc
Route::get('categories', [CategoryController::class, 'index'])->name('api.categories.index');
Route::get('categories/{id}', [CategoryController::class, 'show'])->name('api.categories.show');
Route::post('categories', [CategoryController::class, 'store'])->name('api.categories.store');
Route::put('categories/{id}', [CategoryController::class, 'update'])->name('api.categories.update');
Route::delete('categories/{id}', [CategoryController::class, 'destroy'])->name('api.categories.destroy');


// Lấy danh sách bafi viết
Route::get('posts', [PostController::class, 'index'])->name('api.posts.index');
Route::get('posts/{id}', [PostController::class, 'show'])->name('api.posts.show');
Route::post('posts', [PostController::class, 'store'])->name('api.posts.store');
Route::put('posts/{id}', [PostController::class, 'update'])->name('api.posts.update');
Route::delete('posts/{id}', [PostController::class, 'destroy'])->name('api.posts.destroy');

// Lấy danh sách danh muc



Route::get('category-products', [CategoryProductController::class, 'index'])->name('api.category-products.index');
Route::get('category-products/{id}', [CategoryProductController::class, 'show'])->name('api.category-products.show');
Route::post('category-products', [CategoryProductController::class, 'store'])->name('api.category-products.store');
Route::put('category-products/{id}', [CategoryProductController::class, 'update'])->name('api.category-products.update');
Route::delete('category-products/{id}', [CategoryProductController::class, 'destroy'])->name('api.category-products.destroy');



Route::post('/tokens/create', function (Request $request) {
    $user = User::firstOrFail();
    $token = $user->createToken('token');

    return ['token' => $token->plainTextToken];
});




Route::middleware('auth:sanctum')->group(function () {

    Route::post('/logout', [LoginController::class, 'logout'])->name('api.logout');
    Route::get('/me', [ProfileController::class, 'index'])->name('api.profile');

    


});

Route::post('/login', [LoginController::class, 'login'])->name('api.login');
Route::post('/register', [RegistrationController::class, 'register'])->name('api.register');

Route::get('orders', [OrderController::class, 'index'])->name('api.orders.index');
Route::post('orders', [OrderController::class, 'store'])->name('api.orders.store');
Route::get('orders/{id}', [OrderController::class, 'show'])->name('api.orders.show');


// Route::delete('product', [ProductController::class, 'index'])->name('api.product');
Route::get('products', [ProductController::class, 'index'])->name('api.products.index');
Route::get('products/{id}', [ProductController::class, 'show'])->name('api.products.show');
Route::post('/products/{product}/check-inventory', [ProductController::class, 'checkInventory'])
    ->name('api.products.check-inventory');


Route::get('category-products', [CategoryProductController::class, 'index'])->name('api.category-products.index');
Route::get('category-products/{categoryProduct}', [CategoryProductController::class, 'show'])->name('api.category-products.show');
