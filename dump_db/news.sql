-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 22, 2023 lúc 02:06 PM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `news`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '2023-04-25 07:50:49', '$2y$10$iAO6MrR8ch76kJQ.Q1pDCepj2DJ4YQGn7rHI0Gv9do8aJRMfdbTpG', 'R6Qfj9jbbl', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(2, 'Employee', 'employee@gmail.com', '2023-04-25 07:50:49', '$2y$10$rKE8.x0y8hMVn12G7v9rQOHEhqCa0UrNyeTA43/1s7RAqrpTYQqQy', 'Pq0AdPWumN', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(3, 'Test', 'test@gmail.com', '2023-04-25 07:50:49', '$2y$10$76YV3VMKg8q3ET0n3bem8e7f3N33VzAt.AGrrKLIFnVO27IO0.JOC', 'PgjxRO53lO', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bao_tuoi_tre', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(2, 'Bao_24h', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(3, 'Bao_lao_dong', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(4, 'Eli O\'Hara', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(5, 'Zachary Kemmer', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(6, 'Mrs. Aglae Weber', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `content`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 11, 'Dormouse. \'Write that down,\' the King had said that day. \'A likely story indeed!\' said Alice, \'it\'s very easy to know when the White Rabbit, jumping up and straightening itself out again, and put it.', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(2, 1, 12, 'King, the Queen, in a sulky tone, as it happens; and if the Queen ordering off her unfortunate guests to execution--once more the pig-baby was sneezing and howling alternately without a cat! It\'s.', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(3, 1, 13, 'I\'m afraid, but you might knock, and I could say if I shall have to go from here?\' \'That depends a good deal frightened by this time.) \'You\'re nothing but a pack of cards: the Knave \'Turn them.', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(4, 2, 14, 'RED rose-tree, and we put a stop to this,\' she said to herself how she would manage it. \'They were learning to draw, you know--\' \'What did they live at the Lizard as she went back to the beginning.', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(5, 2, 15, 'Said cunning old Fury: \"I\'ll try the thing Mock Turtle interrupted, \'if you don\'t know what a Mock Turtle in a tone of great surprise. \'Of course they were\', said the Gryphon: \'I went to school in.', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(6, 2, 16, 'However, this bottle was NOT marked \'poison,\' it is almost certain to disagree with you, sooner or later. However, this bottle was a paper label, with the edge with each hand. \'And now which is.', '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(7, 3, 17, 'And she squeezed herself up on to her that she remained the same thing,\' said the Queen, who had followed him into the air. \'--as far out to sea. So they sat down, and nobody spoke for some while in.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(8, 3, 18, 'The Mouse looked at them with large eyes like a serpent. She had already heard her voice sounded hoarse and strange, and the words came very queer indeed:-- \'\'Tis the voice of the gloves, and she.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(9, 3, 19, 'And took them quite away!\' \'Consider your verdict,\' the King said, with a lobster as a lark, And will talk in contemptuous tones of the accident, all except the Lizard, who seemed to follow, except.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(10, 4, 20, 'King replied. Here the other ladder?--Why, I hadn\'t mentioned Dinah!\' she said to the door. \'Call the next moment a shower of little Alice was not even get her head through the wood. \'If it had VERY.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(11, 4, 21, 'Alice as it is.\' \'Then you may SIT down,\' the King in a confused way, \'Prizes! Prizes!\' Alice had been wandering, when a sharp hiss made her so savage when they arrived, with a teacup in one hand.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(12, 4, 22, 'It quite makes my forehead ache!\' Alice watched the Queen put on her spectacles, and began singing in its sleep \'Twinkle, twinkle, twinkle, twinkle--\' and went on eagerly: \'There is such a curious.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(13, 5, 23, 'Alice, a little way forwards each time and a Long Tale They were just beginning to grow to my right size again; and the words \'DRINK ME,\' but nevertheless she uncorked it and put it right; \'not that.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(14, 5, 24, 'Alice timidly. \'Would you tell me,\' said Alice, swallowing down her anger as well as she could. The next thing is, to get very tired of swimming about here, O Mouse!\' (Alice thought this a good way.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(15, 5, 25, 'Alice; \'I might as well go in at the Caterpillar\'s making such VERY short remarks, and she trembled till she shook the house, and the blades of grass, but she stopped hastily, for the baby, the.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(16, 6, 26, 'Alice noticed, had powdered hair that curled all over with fright. \'Oh, I beg your pardon!\' she exclaimed in a tone of the baby, and not to make out at all like the look of the sense, and the words.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(17, 6, 27, 'Five! Don\'t go splashing paint over me like a writing-desk?\' \'Come, we shall have to go with the words a little, half expecting to see if she were looking up into the roof off.\' After a time there.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(18, 6, 28, 'Ugh, Serpent!\' \'But I\'m not the same, shedding gallons of tears, until there was hardly room for this, and Alice was rather glad there WAS no one else seemed inclined to say when I got up in a VERY.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(19, 7, 29, 'Alice replied in a sort of lullaby to it as you go on? It\'s by far the most confusing thing I know. Silence all round, if you don\'t know what a delightful thing a Lobster Quadrille The Mock Turtle.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(20, 7, 30, 'Caterpillar called after it; and as the hall was very hot, she kept on good terms with him, he\'d do almost anything you liked with the Lory, as soon as there seemed to Alice with one finger for the.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(21, 7, 31, 'Alice. \'I\'ve read that in the world am I? Ah, THAT\'S the great question certainly was, what? Alice looked all round her, about the reason is--\' here the Mock Turtle replied; \'and then the Rabbit\'s.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(22, 8, 32, 'Gryphon. \'I mean, what makes them sour--and camomile that makes you forget to talk. I can\'t take more.\' \'You mean you can\'t help it,\' she thought, and rightly too, that very few little girls of her.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(23, 8, 33, 'Duchess and the small ones choked and had just begun to repeat it, but her head on her toes when they liked, and left foot, so as to bring but one; Bill\'s got to go through next walking about at the.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(24, 8, 34, 'Do you think you\'re changed, do you?\' \'I\'m afraid I am, sir,\' said Alice; \'but a grin without a cat! It\'s the most interesting, and perhaps as this before, never! And I declare it\'s too bad, that it.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(25, 9, 35, 'Hatter. He had been found and handed them round as prizes. There was a table set out under a tree a few minutes, and began to repeat it, when a sharp hiss made her next remark. \'Then the eleventh.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(26, 9, 36, 'SAID was, \'Why is a very difficult game indeed. The players all played at once took up the fan and gloves, and, as a lark, And will talk in contemptuous tones of her voice, and see that she still.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(27, 9, 37, 'I wonder?\' As she said this, she looked up, and began bowing to the tarts on the stairs. Alice knew it was only the pepper that had slipped in like herself. \'Would it be of any good reason, and as.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(28, 10, 38, 'I think?\' he said in a tone of great relief. \'Now at OURS they had settled down again, the Dodo replied very politely, \'for I can\'t remember,\' said the sage, as he spoke, \'we were trying--\' \'I see!\'.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(29, 10, 39, 'Alice said very politely, \'if I had our Dinah here, I know all sorts of little birds and animals that had made the whole pack rose up into the sea, some children digging in the flurry of the wood.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(30, 10, 40, 'Alice ventured to say. \'What is it?\' Alice panted as she heard a voice sometimes choked with sobs, to sing \"Twinkle, twinkle, little bat! How I wonder if I chose,\' the Duchess said after a minute or.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(31, 11, 41, 'I suppose?\' \'Yes,\' said Alice, \'but I know THAT well enough; don\'t be nervous, or I\'ll have you executed on the slate. \'Herald, read the accusation!\' said the Cat: \'we\'re all mad here. I\'m mad.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(32, 11, 42, 'Alice did not get hold of this sort in her own ears for having cheated herself in a sulky tone; \'Seven jogged my elbow.\' On which Seven looked up eagerly, half hoping that the Gryphon at the.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(33, 11, 43, 'Cheshire cat,\' said the Dormouse; \'--well in.\' This answer so confused poor Alice, and she felt that this could not stand, and she felt that she was holding, and she thought to herself how this same.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(34, 12, 44, 'Mock Turtle, and said nothing. \'When we were little,\' the Mock Turtle angrily: \'really you are very dull!\' \'You ought to be a grin, and she felt very glad that it was a little irritated at the.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(35, 12, 45, 'Pray, what is the use of a tree a few minutes to see if she could not help thinking there MUST be more to come, so she waited. The Gryphon sat up and rubbed its eyes: then it chuckled. \'What fun!\'.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(36, 12, 46, 'Lobster Quadrille?\' the Gryphon said, in a very small cake, on which the cook took the hookah out of its mouth and began an account of the shelves as she had caught the flamingo and brought it back.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(37, 13, 47, 'King. \'When did you ever eat a bat?\' when suddenly, thump! thump! down she came upon a Gryphon, lying fast asleep in the window?\' \'Sure, it\'s an arm for all that.\' \'With extras?\' asked the Mock.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(38, 13, 48, 'Dinah at you!\' There was nothing else to do, and in despair she put it. She stretched herself up on to her lips. \'I know SOMETHING interesting is sure to make SOME change in my life!\' Just as she.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(39, 13, 49, 'Bill,\' thought Alice,) \'Well, I shan\'t go, at any rate: go and live in that case I can find them.\' As she said to herself \'This is Bill,\' she gave one sharp kick, and waited to see how he can.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(40, 14, 50, 'So she began shrinking directly. As soon as she could, and waited to see you any more!\' And here poor Alice began telling them her adventures from the Gryphon, and the Gryphon as if it had been, it.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(41, 14, 51, 'But the snail replied \"Too far, too far!\" and gave a little house in it a very respectful tone, but frowning and making quite a crowd of little Alice was beginning very angrily, but the Dodo.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(42, 14, 52, 'You MUST have meant some mischief, or else you\'d have signed your name like an arrow. The Cat\'s head began fading away the moment she appeared on the spot.\' This did not like the Mock Turtle sang.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(43, 15, 53, 'Alice, and looking anxiously about her. \'Oh, do let me help to undo it!\' \'I shall do nothing of the door began sneezing all at once. The Dormouse slowly opened his eyes were nearly out of breath.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(44, 15, 54, 'The Rabbit Sends in a shrill, loud voice, and the game was in a minute. Alice began in a hoarse, feeble voice: \'I heard every word you fellows were saying.\' \'Tell us a story.\' \'I\'m afraid I don\'t.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(45, 15, 55, 'Hatter, and, just as well be at school at once.\' However, she soon found out a new idea to Alice, that she remained the same thing a bit!\' said the Gryphon, \'you first form into a large caterpillar.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(46, 16, 56, 'King. \'Nothing whatever,\' said Alice. \'Why, you don\'t like it, yer honour, at all, at all!\' \'Do as I used--and I don\'t put my arm round your waist,\' the Duchess was VERY ugly; and secondly, because.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(47, 16, 57, 'Hatter: \'as the things between whiles.\' \'Then you should say \"With what porpoise?\"\' \'Don\'t you mean by that?\' said the Cat. \'I said pig,\' replied Alice; \'and I wish I hadn\'t drunk quite so much!\'.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(48, 16, 58, 'Mad Tea-Party There was no time she\'d have everybody executed, all round. (It was this last word with such sudden violence that Alice had no reason to be patted on the Duchess\'s voice died away.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(49, 17, 59, 'Mock Turtle, \'they--you\'ve seen them, of course?\' \'Yes,\' said Alice, in a moment that it might appear to others that what you were me?\' \'Well, perhaps your feelings may be ONE.\' \'One, indeed!\' said.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(50, 17, 60, 'Alice: \'three inches is such a curious plan!\' exclaimed Alice. \'That\'s the reason and all would change to tinkling sheep-bells, and the blades of grass, but she felt that she remained the same size.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(51, 17, 61, 'I almost think I could, if I only knew how to begin.\' For, you see, so many different sizes in a confused way, \'Prizes! Prizes!\' Alice had never had fits, my dear, and that makes them.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(52, 18, 62, 'Queen. \'I never could abide figures!\' And with that she did not like the tone of the reeds--the rattling teacups would change to dull reality--the grass would be offended again. \'Mine is a very.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(53, 18, 63, 'The first thing I\'ve got to see it quite plainly through the doorway; \'and even if my head would go round a deal too far off to other parts of the wood for fear of killing somebody, so managed to.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(54, 18, 64, 'I beat him when he pleases!\' CHORUS. \'Wow! wow! wow!\' While the Duchess said in a wondering tone. \'Why, what are they doing?\' Alice whispered to the other was sitting on a little hot tea upon its.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(55, 19, 65, 'IX. The Mock Turtle\'s heavy sobs. Lastly, she pictured to herself \'Now I can do no more, whatever happens. What WILL become of me? They\'re dreadfully fond of pretending to be sure, this generally.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(56, 19, 66, 'Hatter hurriedly left the court, she said to herself. \'Shy, they seem to see you again, you dear old thing!\' said the King replied. Here the Queen in a low, weak voice. \'Now, I give it up,\' Alice.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(57, 19, 67, 'I can say.\' This was not a VERY turn-up nose, much more like a star-fish,\' thought Alice. \'Now we shall have some fun now!\' thought Alice. \'I mean what I was sent for.\' \'You ought to be ashamed of.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(58, 20, 68, 'Soup! Soup of the bottle was a queer-shaped little creature, and held out its arms folded, frowning like a telescope! I think I could, if I shall only look up in her haste, she had sat down and make.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(59, 20, 69, 'Hatter, with an M, such as mouse-traps, and the arm that was sitting between them, fast asleep, and the words a little, and then I\'ll tell you how it was labelled \'ORANGE MARMALADE\', but to open her.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(60, 20, 70, 'I should like to show you! A little bright-eyed terrier, you know, with oh, such long ringlets, and mine doesn\'t go in ringlets at all; however, she again heard a little pattering of feet on the top.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(61, 21, 71, 'I had it written down: but I hadn\'t mentioned Dinah!\' she said these words her foot slipped, and in his throat,\' said the Mock Turtle repeated thoughtfully. \'I should like to try the first minute or.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(62, 21, 72, 'Cat in a very curious to know your history, she do.\' \'I\'ll tell it her,\' said the Hatter. He came in with a large mustard-mine near here. And the muscular strength, which it gave to my boy, I beat.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(63, 21, 73, 'I don\'t keep the same solemn tone, \'For the Duchess. An invitation for the next witness was the matter with it. There was a most extraordinary noise going on within--a constant howling and sneezing.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(64, 22, 74, 'I hate cats and dogs.\' It was the first question, you know.\' He was an old conger-eel, that used to do:-- \'How doth the little--\"\' and she felt that it was the cat.) \'I hope they\'ll remember her.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(65, 22, 75, 'Dormouse shall!\' they both sat silent and looked at the house, quite forgetting that she was peering about anxiously among the distant green leaves. As there seemed to listen, the whole she thought.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(66, 22, 76, 'Queen was silent. The Dormouse had closed its eyes by this time?\' she said to the tarts on the bank--the birds with draggled feathers, the animals with their heads!\' and the fall was over. Alice was.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(67, 23, 77, 'And yet you incessantly stand on your shoes and stockings for you now, dears? I\'m sure _I_ shan\'t be beheaded!\' said Alice, \'but I must go by the way, and then said \'The fourth.\' \'Two days wrong!\'.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(68, 23, 78, 'Pigeon; \'but I must be the best cat in the same thing as \"I eat what I used to it in a melancholy way, being quite unable to move. She soon got it out to the Mock Turtle, suddenly dropping his.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(69, 23, 79, 'Alice asked in a low voice. \'Not at all,\' said the young Crab, a little timidly, \'why you are painting those roses?\' Five and Seven said nothing, but looked at the place of the e--e--evening.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(70, 24, 80, 'May it won\'t be raving mad--at least not so mad as it was talking in a melancholy air, and, after folding his arms and legs in all my life, never!\' They had not long to doubt, for the next moment a.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(71, 24, 81, 'Dormouse shall!\' they both bowed low, and their curls got entangled together. Alice was not much larger than a pig, and she jumped up on tiptoe, and peeped over the verses to himself: \'\"WE KNOW IT.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(72, 24, 82, 'Alice said; but was dreadfully puzzled by the White Rabbit with pink eyes ran close by her. There was a good opportunity for showing off her knowledge, as there was no longer to be seen--everything.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(73, 25, 83, 'Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, but in a piteous tone. And she began again: \'Ou est ma chatte?\' which was a dispute going on within--a constant.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(74, 25, 84, 'Alice, in a trembling voice:-- \'I passed by his garden, and I had to stop and untwist it. After a minute or two, looking for the fan and gloves, and, as they used to come out among the party. Some.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(75, 25, 85, 'Latin Grammar, \'A mouse--of a mouse--to a mouse--a mouse--O mouse!\') The Mouse did not like the name: however, it only grinned when it had grown up,\' she said to herself, as well she might, what a.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(76, 26, 86, 'Twinkle, twinkle--\"\' Here the Dormouse again, so that by the fire, licking her paws and washing her face--and she is only a child!\' The Queen turned crimson with fury, and, after glaring at her.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(77, 26, 87, 'I should think it so VERY wide, but she stopped hastily, for the pool a little timidly: \'but it\'s no use in waiting by the Queen left off, quite out of that is--\"Be what you like,\' said the Mock.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(78, 26, 88, 'Duchess! The Duchess! Oh my fur and whiskers! She\'ll get me executed, as sure as ferrets are ferrets! Where CAN I have dropped them, I wonder?\' As she said to herself how this same little sister of.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(79, 27, 89, 'I don\'t want to go! Let me think: was I the same when I was thinking I should have croqueted the Queen\'s absence, and were resting in the act of crawling away: besides all this, there was a little.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(80, 27, 90, 'Duchess, \'as pigs have to turn round on its axis--\' \'Talking of axes,\' said the others. \'We must burn the house if it had come back in their mouths; and the three gardeners instantly jumped up, and.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(81, 27, 91, 'Mock Turtle, \'but if they do, why then they\'re a kind of thing that would happen: \'\"Miss Alice! Come here directly, and get ready to sink into the wood. \'If it had made. \'He took me for asking! No.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(82, 28, 92, 'I THINK I can kick a little!\' She drew her foot as far as they lay sprawling about, reminding her very much to-night, I should think!\' (Dinah was the first verse,\' said the Queen. \'Their heads are.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(83, 28, 93, 'I eat one of them.\' In another minute there was a treacle-well.\' \'There\'s no sort of way to change the subject,\' the March Hare said to herself, and fanned herself with one of its voice. \'Back to.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(84, 28, 94, 'Alice did not like to see it trot away quietly into the court, \'Bring me the truth: did you call it sad?\' And she went down to nine inches high. CHAPTER VI. Pig and Pepper For a minute or two. \'They.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(85, 29, 95, 'And so it was only a pack of cards, after all. I needn\'t be so proud as all that.\' \'Well, it\'s got no business there, at any rate a book written about me, that there was no time to see how he did.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(86, 29, 96, 'Let me see: I\'ll give them a railway station.) However, she soon made out that one of them bowed low. \'Would you like to show you! A little bright-eyed terrier, you know, as we needn\'t try to find.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(87, 29, 97, 'However, \'jury-men\' would have appeared to them to be nothing but out-of-the-way things to happen, that it is!\' As she said to herself, in a melancholy tone: \'it doesn\'t seem to have him with them,\'.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(88, 30, 98, 'I got up this morning, but I THINK I can do no more, whatever happens. What WILL become of you? I gave her answer. \'They\'re done with blacking, I believe.\' \'Boots and shoes under the hedge. In.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(89, 30, 99, 'WHAT?\' said the Hatter. \'Stolen!\' the King say in a minute. Alice began telling them her adventures from the sky! Ugh, Serpent!\' \'But I\'m NOT a serpent!\' said Alice in a tone of great curiosity.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL),
(90, 30, 100, 'Cat. \'I don\'t see how he did not like to drop the jar for fear of their wits!\' So she set to work throwing everything within her reach at the house, and found that, as nearly as large as the large.', '2023-04-25 07:50:50', '2023-04-25 07:50:50', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_03_15_062529_create_categories_table', 1),
(6, '2023_03_15_062656_create_posts_table', 1),
(7, '2023_03_15_062745_create_comments_table', 1),
(8, '2023_04_13_072340_create_admins_table', 1),
(9, '2023_04_15_145147_add_column_is_feature_to_posts_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_feature` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `name`, `content`, `thumbnail`, `is_feature`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 'Top 7 Địa chỉ mua thức ăn cá Koi tốt nhất tại Hà Nội', 'Cá Koi vốn là dòng cá mang biểu tượng của sự may mắn, tài lộc do người Nhật quan niệm dần được phổ biến ra các nước trong đó có Việt Nam. Nhờ vậy mà những năm gần đây thú chơi cá Koi ngày càng phổ biến. Tuy việc nuôi cá Koi không quá khó nhưng cũng cần chú ý đến một số điểm, trong đó chọn thức ăn cho cá Koi là việc không thể bỏ qua. Hãy cùng Toplist tìm hiểu những địa chỉ mua thức ăn cá Koi tốt nhất tại Hà Nội nhé!', 'https://media.topic.vn/articles/2023/dia-chi-mua-thuc-an-ca-koi-tot-nhat-tai-ha-noi-61908e.jpg', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(2, 4, 'Top 7 Quán ốc ngon nhất tỉnh Hải Dương', 'Ẩm thực luôn là một phạm trù rộng lớn và ngày càng phát triển. Nếu bạn là một người có niềm đam mê với các món ăn và luôn muốn tìm đến những địa chỉ ăn ngon thì hãy cùng Toplist đi đến những quán ốc ngon nhất tỉnh Hải Dương để khám phá ngay nhé!', 'https://media.topic.vn/articles/2023/quan-oc-ngon-nhat-tinh-hai-duong-thumb.jpg', 1, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(3, 4, 'Top 5 Địa chỉ gội đầu dưỡng sinh tốt nhất tỉnh Thái Bình', 'Sau những giờ làm việc mệt mỏi, chúng ta rất cần thư giãn với những liệu pháp hữu ích, trong đó gội đầu dưỡng sinh là một sự lựa chọn tuyệt vời. Bạn ở Thái Bình đang cần tìm một địa chỉ gội đầu dưỡng sinh chuyên nghiệp thì hãy cùng Toplist tìm hiểu thông qua bài viết này nhé.Chính sự đả thông huyệt đạo giúp lưu thông khí huyết của gội đầu dưỡng sinh đã giúp phương pháp này trở thành một trong những kỹ nghệ gội đầu hiệu quả và được quan tâm nhiều nhất hiện tại. Nếu bạn ở Thái Bình cần tìm một địa chỉ gội đầu dưỡng sinh thì hãy ghé W Cosmetics & Spa chỉ với giá 150k bạn đã có 90 phút thư giãn tuyệt vời.', 'https://media.topic.vn/articles/2023/dia-chi-goi-dau-duong-sinh-tot-nhat-tinh-thai-binh-thumb.jpg', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(4, 4, 'Top 3 Địa chỉ mua áo phao cứu hộ uy tín nhất Hải Phòng', 'Áo phao cứu sinh hay gọi là áo phao cứu hộ, chúng là một trong đồ bảo hộ lao động vô cùng quan trọng đến những người thường xuyên tiếp xúc với nước khi đi biển, tập bơi, cứu hộ những người đang gặp nguy cơ đuối nước. Nhưng đâu mới là nơi cung cấp những chiếc áo phao chất lượng, hãy cùng Toplist tìm hiểu dưới bài viết dưới đây nhé!', 'https://media.topic.vn/articles/2023/dia-chi-mua-ao-phao-cuu-ho-uy-tin-nhat-hai-phong-thumb.jpg', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(5, 4, 'Top 4 Dịch vụ thiết kế nhà hàng, quán cafe tốt nhất tỉnh Ninh Bình', 'Ninh Bình là một tỉnh có thế mạnh về du lịch, chính vì thế các dịch vụ nhà hàng, cafe, trà sữa đều phát triển rất mạnh. Đây là cơ hội cho chủ đầu tư xây dựng mô hình kinh doanh của mình. Mỗi quán cafe hay nhà hàng muốn kinh doanh phát triển cần phải có một không gian thật đẹp, khác biệt và mới lạ để tạo ấn tượng với thực khách. Nếu bạn đang muốn mở nhà hàng hay quán cafe để kinh doanh nhưng chưa biết nên thiết kế sao cho độc đáo thì hãy tham khảo các địa chỉ thiết kế nhà hàng, quán cafe tốt nhất Ninh Bình trong bài viết dưới đây nhé.', 'https://media.topic.vn/articles/2023/dich-vu-thiet-ke-nha-hang-quan-cafe-tot-nhat-tinh-ninh-binh-thumb.jpg', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(6, 4, 'Top 11 Sự kiện đạp xe, chạy bộ, bơi biển nổi bật nhất năm 2023', 'Song song với việc phát triển tinh thần lạc quan thì phát triển thể lực cũng là điều quan trọng để có một sức khỏe tốt. Các hoạt động thể lực như đạp xe, chạy bộ, bơi biển được đánh giá là các hoạt động giúp tăng cường sức khỏe, rèn luyện phản xạ và khả năng tư duy, giảm stress. Các vận động viên thể thao thường chọn ba môn phối hợp gồm chạy bộ, bơi và đạp xe để thử thách giới hạn của bản thân. Nếu bạn cũng dành tình yêu đặc biệt và quan tâm đến các hoạt động này thì hãy tham khảo bài viết dưới đây của Toplist nhé.', 'https://media.topic.vn/articles/2023/su-kien-dap-xe-chay-bo-boi-bien-noi-bat-nhat-nam-2023-thumb.jpg', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(7, 4, 'Top 8 Địa chỉ bán máy lọc không khí ô tô uy tín tại Hà Nội', 'Môi trường nào cũng luôn tồn tại rất nhiều bụi bẩn, chất độc hại, ngay cả trên xe ô tô của bạn cũng vậy. Máy lọc không khí trên ô tô là vật dụng cần thiết nhất đối với những người hay di chuyển bằng phương tiện này. Sử dụng chiếc máy này, không chỉ lọc sạch được bụi bẩn, mà còn hạn chế được tình trạng say xe. Với những công dụng hữu ích như vậy, càng ngày máy lọc không khí càng được ưa chuộng và sử dụng nhiều. Cùng Toplist tìm hiểu những địa chỉ bán máy lọc không khí ô tô uy tín nhất tại Hà Nội nhé.', 'https://media.topic.vn/articles/2023/dia-chi-ban-may-loc-khong-khi-o-to-uy-tin-tai-ha-noi-thumb.jpg', 1, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(8, 4, 'Top 5 Địa chỉ mua áo chỉnh hình uy tín nhất Hà Nội', 'Áo chỉnh hình hay nẹp vùng thân là dụng cụ hỗ trợ vùng thân mình nó tác động gián tiếp lên cột sống, khung chậu, đai vai,... Áo chỉnh hình thường sử dụng trong những trường hợp cố định sau chấn thương, cố định sau phẫu thuật hay duy trì tư thế do sai lệch. Trong bài viết sau đây, Toplist sẽ chia sẻ đến bạn một số địa chỉ mua áo chỉnh hình uy tín nhất Hà Nội giúp bạn tham khảo.', 'https://media.topic.vn/articles/2023/dia-chi-mua-ao-chinh-hinh-uy-tin-nhat-ha-noi-thumb.jpg', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(9, 4, 'Burley Gulgowski', 'She gave me a good deal worse off than before, as the large birds complained that they must needs come wriggling down from the Gryphon, and, taking Alice by the carrier,\' she thought; \'and how funny.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(10, 4, 'Mr. Arnaldo Runolfsson', 'White Rabbit hurried by--the frightened Mouse splashed his way through the neighbouring pool--she could hear him sighing as if it likes.\' \'I\'d rather finish my tea,\' said the Dormouse shall!\' they.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(11, 5, 'Mrs. Maddison Kreiger', 'Alice, \'when one wasn\'t always growing larger and smaller, and being ordered about in all directions, \'just like a wild beast, screamed \'Off with her head!\' the Queen shrieked out. \'Behead that.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(12, 5, 'Simeon Wisozk', 'I am, sir,\' said Alice; \'you needn\'t be afraid of them!\' \'And who is to find herself still in sight, hurrying down it. There was a general chorus of voices asked. \'Why, SHE, of course,\' the Mock.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(13, 5, 'Henri Bailey', 'If she should push the matter with it. There could be beheaded, and that in the pool was getting so used to read fairy-tales, I fancied that kind of rule, \'and vinegar that makes them sour--and.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(14, 5, 'Ocie Bradtke', 'It\'s enough to look over their shoulders, that all the while, and fighting for the immediate adoption of more broken glass.) \'Now tell me, please, which way she put her hand on the shingle--will you.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(15, 5, 'Georgette Koss MD', 'The hedgehog was engaged in a hurry: a large one, but it was very hot, she kept on good terms with him, he\'d do almost anything you liked with the Mouse had changed his mind, and was in livery.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(16, 5, 'Mrs. Maryam O\'Kon', 'Alice\'s head. \'Is that the meeting adjourn, for the pool as it happens; and if it please your Majesty!\' the Duchess by this time.) \'You\'re nothing but a pack of cards: the Knave \'Turn them over!\'.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(17, 5, 'Mr. Conner Langosh', 'Suppress him! Pinch him! Off with his head!\' or \'Off with their heads down! I am now? That\'ll be a letter, after all: it\'s a French mouse, come over with diamonds, and walked off; the Dormouse.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(18, 5, 'Dino Effertz III', 'Caterpillar took the least notice of her head made her next remark. \'Then the words all coming different, and then the other, and growing sometimes taller and sometimes she scolded herself so.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(19, 5, 'Selina Haag', 'Lizard, Bill, was in the sun. (IF you don\'t know the way I ought to have it explained,\' said the March Hare was said to herself in the court!\' and the Panther were sharing a pie--\' [later editions.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(20, 5, 'Mae Goyette PhD', 'At this moment Alice felt a little more conversation with her face like the three gardeners instantly threw themselves flat upon their faces. There was no \'One, two, three, and away,\' but they were.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(21, 6, 'Mr. Bobby Osinski', 'How she longed to get rather sleepy, and went down on their slates, and then Alice put down her flamingo, and began by taking the little passage: and THEN--she found herself safe in a shrill.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(22, 6, 'Crawford Bogisich', 'Mock Turtle replied; \'and then the other, and growing sometimes taller and sometimes shorter, until she had peeped into the darkness as hard as he spoke. \'UNimportant, of course, Alice could only.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(23, 6, 'Cathryn Bashirian', 'So Alice began in a moment: she looked at her, and the baby at her own mind (as well as she could, and waited to see if there were three little sisters,\' the Dormouse crossed the court, she said.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(24, 6, 'Estel Stokes', 'Hatter: \'it\'s very interesting. I never heard it before,\' said Alice,) and round goes the clock in a bit.\' \'Perhaps it hasn\'t one,\' Alice ventured to taste it, and kept doubling itself up very.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(25, 6, 'Kenyon Kunze', 'THERE again!\' said Alice indignantly. \'Ah! then yours wasn\'t a really good school,\' said the Mock Turtle in a languid, sleepy voice. \'Who are YOU?\' Which brought them back again to the three.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(26, 6, 'Hillary Lesch', 'The Footman seemed to have it explained,\' said the Duck. \'Found IT,\' the Mouse was bristling all over, and she went on, spreading out the proper way of expressing yourself.\' The baby grunted again.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(27, 6, 'Adam Hayes', 'I am, sir,\' said Alice; \'it\'s laid for a long sleep you\'ve had!\' \'Oh, I\'ve had such a nice little dog near our house I should think you might like to see it written up somewhere.\' Down, down, down.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(28, 6, 'Dr. Milton Morissette', 'As a duck with its tongue hanging out of the trees as well be at school at once.\' However, she got into a small passage, not much like keeping so close to her: its face to see the Hatter hurriedly.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(29, 6, 'Jameson Cruickshank', 'KNOW IT TO BE TRUE--\" that\'s the jury, in a low voice, to the fifth bend, I think?\' he said do. Alice looked all round the neck of the e--e--evening, Beautiful, beautiful Soup!\' CHAPTER XI. Who.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL),
(30, 6, 'Frederik Bashirian', 'WHAT?\' said the Rabbit asked. \'No, I give it up,\' Alice replied: \'what\'s the answer?\' \'I haven\'t the least idea what to do THAT in a confused way, \'Prizes! Prizes!\' Alice had no very clear notion.', 'https://picsum.photos/200/300', 0, '2023-04-25 07:50:49', '2023-04-25 07:50:49', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shane Kohler', 'bhilpert@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DO4jIeBR6CBOr2FE5GyoJkBq8bjB4py1A2fa9E2UuYvILyzv8qWlBNdGVPJh', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(2, 'Andrew Fisher', 'hettinger.albina@example.com', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3uSqPs7r4t', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(3, 'Prof. Amani Bayer DDS', 'domenic50@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0H1LAjE3OD', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(4, 'Nakia Graham', 'wellington50@example.org', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4gac5ieORc', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(5, 'Jeffry Hansen', 'bode.leila@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'sBtDp8JSNv', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(6, 'Anibal Mayert', 'volson@example.org', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YhjL5hK22O', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(7, 'Murphy Ullrich', 'acollier@example.com', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LU9DABfSEZ', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(8, 'Ms. Amely Grimes', 'ggraham@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'qMrImdOCgI', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(9, 'Reyes Hettinger', 'bashirian.kyleigh@example.com', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ipzFyuDhTY', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(10, 'Mr. Tyrel Jacobi III', 'jraynor@example.com', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bkL34EXZkQ', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(11, 'Katelyn Wilderman', 'eleazar70@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iBAiMM6aCI', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(12, 'Frederick Blanda II', 'gregoria64@example.org', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'o6CiLN8Exh', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(13, 'D\'angelo Prohaska', 'toney.green@example.org', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'uhPJt6FjFy', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(14, 'Mr. Giuseppe Runolfsdottir PhD', 'tito.rau@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0LHQzyZ6fx', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(15, 'Sierra McCullough', 'bwilderman@example.org', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4jNg4Qyi6c', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(16, 'Dr. Zena Von DDS', 'wglover@example.net', '2023-04-25 07:50:49', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'CZIz5MlkhD', '2023-04-25 07:50:49', '2023-04-25 07:50:49'),
(17, 'Mrs. Marielle Casper III', 'keanu.kuhic@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'cwhXQwSjri', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(18, 'Dr. Juliet Block', 'yasmine.durgan@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aHmieD0qdu', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(19, 'Kennedi Lakin', 'huels.shemar@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jPg4uLi3xT', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(20, 'Danny Reichert III', 'jeramie.berge@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aE3an9f68A', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(21, 'Elwyn Corwin', 'jon16@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dvFL0wlo5t', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(22, 'Prof. Roel Schowalter', 'pierce14@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yHqb9IG8fr', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(23, 'Laron Pacocha', 'layla36@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Q6U3sGuUMQ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(24, 'Dustin Gutmann', 'doug84@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KD7qKznogF', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(25, 'King Conn', 'bernardo.legros@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'RUDNQKRQUD', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(26, 'Mrs. Lorena Balistreri DVM', 'brown42@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'v3pziNDGjQ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(27, 'Hilario Ratke', 'lind.leon@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GtHJTUgqqy', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(28, 'Dr. Quinton Leuschke', 'yjohnston@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'nNsXtjzaCS', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(29, 'Armando Shanahan', 'godfrey62@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Uk8jTGQnxZ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(30, 'Prof. Kameron Morar DDS', 'anita.jaskolski@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'pHC8Ik8egv', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(31, 'Dr. Sandrine Deckow', 'jhermiston@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'A4cltpKv8e', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(32, 'Connor Little', 'belle18@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yzqjeisXAe', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(33, 'Jackeline Corwin', 'paul42@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Xhvy4xcSkL', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(34, 'Rahul O\'Keefe', 'koelpin.leonor@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Hw2BpW61VO', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(35, 'Mrs. Kylie Harber', 'junior17@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kJQQBnHBkX', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(36, 'Ms. June Cassin MD', 'tgrimes@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'kZmKfmYapo', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(37, 'Ethelyn Franecki', 'darion.schaden@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'oots1GKyXC', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(38, 'Kole Konopelski', 'lisandro24@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5DqQdXz0xL', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(39, 'Prof. Waino Wyman', 'tgleason@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'N13GExpTqh', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(40, 'Virginie Tromp', 'wmills@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Bm7steur5f', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(41, 'Remington Goodwin', 'fwelch@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'mrnIzpeKBb', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(42, 'Dr. Ana Brakus', 'kameron95@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '9UORzXNrHG', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(43, 'Bart Kilback', 'watsica.franz@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'goOII7LKt2', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(44, 'Prof. Daniela Will DDS', 'rashawn94@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'orgfiEXXcB', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(45, 'Loy Ernser', 'brooklyn82@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ylKlelUzp2', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(46, 'Dr. Hortense Swaniawski II', 'tshields@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Z3Rl1WXZQ6', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(47, 'Jeanette Hansen', 'wilburn96@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xIDwevcLiO', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(48, 'Cecil Koss', 'adrain.kassulke@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'BAEzrK3bHI', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(49, 'Dr. Vida Fahey MD', 'carey54@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FdkdpdiuTO', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(50, 'Joesph Marquardt', 'fkuhlman@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'HLVAQc2uCN', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(51, 'Yessenia Hauck', 'tturner@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'O10FJzp3FU', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(52, 'Jaqueline O\'Kon DDS', 'rebeka27@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'a18I3g7baJ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(53, 'Prof. Jordy Nolan IV', 'ostokes@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KGpFUKvUfD', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(54, 'Dusty Christiansen DDS', 'johnston.autumn@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EuQat68Znb', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(55, 'Prof. Oscar Mueller IV', 'kemmer.enrique@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'E6vTN2Qu7Q', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(56, 'Reuben Boehm', 'schuppe.frederic@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EN52IQBor4', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(57, 'Geovanni Moen', 'carlos23@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1Qam5Wbmeo', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(58, 'Matilde Prosacco', 'nasir.schinner@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'NJnfJhF4eH', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(59, 'Yoshiko Cartwright', 'dickens.lucy@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'aMZq9xU43x', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(60, 'Barbara Schuster', 'beaulah.okeefe@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GIX7kscPCd', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(61, 'Emmanuelle Gutmann', 'hallie.bartoletti@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3eU8I6cGNO', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(62, 'Dr. Rene Runolfsson', 'eve38@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0DGq9BS0Nf', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(63, 'Christop McClure', 'mollie.klocko@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'iBhi3QGWfG', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(64, 'Marc Friesen V', 'adelle.marquardt@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'egarCuQDMV', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(65, 'Mr. Joshuah Bahringer PhD', 'tremaine.okon@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'hLOTP5p2tM', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(66, 'Bernadine Thompson', 'edmond.auer@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'p8fSMB9Z0h', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(67, 'Donna Lowe', 'tgraham@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'fV5Ne4KZVp', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(68, 'Aryanna Larson', 'johnpaul27@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YcTa0lbJ7N', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(69, 'Humberto Johns', 'fkemmer@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YP9a717YFF', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(70, 'Dr. Maxwell Konopelski II', 'ebrown@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'h91xvFTDcl', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(71, 'Mr. Raheem Leuschke Sr.', 'ehand@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yX66qrd1wm', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(72, 'Dr. Mohammed Batz II', 'rempel.mireya@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XH9KOp4LN5', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(73, 'Prof. Candido Conn DDS', 'waters.nicole@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XuLcoR65dZ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(74, 'Maximo Walter', 'pinkie.gibson@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rDv9nJSXAH', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(75, 'Dr. Makenzie Swift', 'miguel61@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'MAaUop8bz3', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(76, 'Derick Hansen DVM', 'ardith46@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'PtFt6e4gfi', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(77, 'Salvatore Considine', 'crist.patsy@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6DrwPmVldZ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(78, 'Miss Rita Rogahn II', 'gillian54@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'V76Upeh2Ho', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(79, 'Cale Berge', 'maltenwerth@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '37GoDcnTs5', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(80, 'Sonia Keebler', 'charlotte78@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'QscmuY5qza', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(81, 'Mr. Monte Gorczany MD', 'dora47@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UmTbcsythj', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(82, 'Lorna Cummings', 'anicolas@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'T8a6yiEd7B', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(83, 'Valerie Batz', 'vrussel@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5Fla7PlyRZ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(84, 'Anissa Gleason', 'qadams@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'zgAcYp3kOu', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(85, 'Edward Pagac', 'claud37@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'X2QYAXDv7Y', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(86, 'Dr. Kamren Torphy', 'xhalvorson@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'voW9NuBoJP', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(87, 'Brandy Dicki DVM', 'uromaguera@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ezfh9lj6wd', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(88, 'Mr. Johnathan O\'Hara IV', 'huel.jamarcus@example.net', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'UxyUFX9DnI', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(89, 'Lacy Cole', 'morgan.quitzon@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '94qzBB5a1z', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(90, 'Miss Rozella Stanton', 'legros.jamal@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1spN0Pzfs5', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(91, 'Jamaal McClure', 'hollie21@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tOC6NDx8LB', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(92, 'Austen Heller', 'kkuhic@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'KVNo6vuAhj', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(93, 'Coleman Yost', 'terrell20@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ga4jhJ7YFQ', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(94, 'Miss Helen Balistreri', 'nconn@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'd8E3ipBFdM', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(95, 'Tobin Howe', 'asmith@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'R9G9k8Mwls', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(96, 'Daisha Stracke IV', 'wbrown@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XkfPWSJeG8', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(97, 'Richmond Ullrich', 'emertz@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GEqihWuLE6', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(98, 'Miss Rafaela Koch DDS', 'vlesch@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'bmPF7ysRSY', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(99, 'Marvin Sporer', 'crodriguez@example.org', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YtuoEXTiQ5', '2023-04-25 07:50:50', '2023-04-25 07:50:50'),
(100, 'Lois Adams', 'phills@example.com', '2023-04-25 07:50:50', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XdhmXI4ibJ', '2023-04-25 07:50:50', '2023-04-25 07:50:50');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_category_id_foreign` (`category_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
