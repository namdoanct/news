<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class MyFeatureTest extends TestCase
{
    use RefreshDatabase;

    // public function testLogin()
    // {
    //     // Arrange
    //     $user = User::factory()->create();

    //     // Act
    //     $response = $this->post('/login', [
    //         'email' => $user->email,
    //         'password' => 'password',
    //     ]);

    //     // Assert
    //     $response->assertStatus(200);
    //     $response->assertSeeText('Welcome, ' . $user->name);
    // }

    //  public function testUserCanViewLogin()
    // {
    //     $response = $this->get('/login');

    //     $response->assertStatus(200);
    //     $response->assertViewIs('auth.login')->assertSee('Login');
    // }

    public function testCanLogin()
    {
        $this->assertGuest();
        $user = User::factory()->create([
            'password' => bcrypt('feature'),
        ]);

        $this->post('/login', [
            'email' => $user->email,
            'password' => 'feature',
        ])
            ->assertStatus(302);
            // ->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);
    }


    // public function testUserCannotLoginWithIncorrectPassword()
    // {
    //     $user = User::factory()->make([
    //         'password' => bcrypt('laravel'),
    //     ]);

    //     $response = $this->from('/login')->post('/login', [
    //         'email' => $user->email,
    //         'password' => 'invalid-password',
    //     ]);

    //     $response->assertRedirect('/login');
    //     $response->assertSessionHasErrors('email');
    //     $this->assertTrue(session()->hasOldInput('email'));
    //     $this->assertFalse(session()->hasOldInput('password'));
    //     $this->assertGuest();
    // }



    // public function testCanLogin()
    // {
    //     $this->assertGuest();
    //     $user = User::factory()->create([
    //         'password' => bcrypt('feature'),
    //     ]);

    //     $this->post('/login', [
    //         'email' => $user->email,
    //         'password' => 'feature',
    //     ])
    //         ->assertStatus(302)
    //         ->assertRedirect('/home');
    //     $this->assertAuthenticatedAs($user);
    // }

        public function testUserCanViewRegister()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
        $response->assertViewIs('auth.register')->assertSee('register');
    }


    public function testCanRegister()
    {
        $this->assertGuest();
        $user = User::factory()->make();

        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'feature',
            'password_confirmation' => 'feature'
        ]);

        $response->assertStatus(302)->assertRedirect('/home');
        $this->assertAuthenticated();
    }

    public function testCannotRegister()
    {
        $this->assertGuest();
        $user = User::factory()->make();

        $response = $this->post('/register', [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'feature',
            'password_confirmation' => 'feature'
        ]);

        $response->assertStatus(200)->assertRedirect('/home');
        $this->assertAuthenticated();
    }
    // public function testUserCanViewRegister()
    // {
    //     $response = $this->get('/register');

    //     $response->assertStatus(200);
    //     $response->assertViewIs('auth.register')->assertSee('register');
    // }


}
