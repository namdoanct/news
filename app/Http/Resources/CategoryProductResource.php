<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        // dd($this->products);
        // return [
        //     'id' => $this->id,
        //     'name' => $this->name,
        //     'created_at' => $this->created_at,
        //     'updated_at' => $this->updated_at,
        //     'products' => ProductSimpleResource::collection($this->products),
        // ];
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'products' => ProductResource::collection($this->products),
            'created' => $this->created,
            'updated' => $this->updated,

        ];
        if(!empty($this->limitProducts)){
            $data['products'] = ProductResource::collection($this->limitProducts);
        }
        return $data;
    }
}
