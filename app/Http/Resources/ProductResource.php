<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        // dd($this->category);
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'content' => $this->content,
            'thumbnail' => config('app.url') . $this->thumbnail,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
        if (!empty($this->category)) {
            // $data = array_merge($data, [
            //     'id' => $this->category->id,
            //     'name' => $this->category->name,
            //     'created_at' => $this->category->created_at,
            //     'updated_at' => $this->category->updated_at,
            // ]);

            $data['category'] = [
                 'id' => $this->category->id,
                'name' => $this->category->name,
                // 'created_at' => $this->category->created_at,
                // 'updated_at' => $this->category->updated_at,
            ];
            // dd($data);
        }
        return $data;

        // resource trong laravel là gì
        // resource là một cách để chuyển đổi dữ liệu của model thành dữ liệu của api


        // 
    }
}
