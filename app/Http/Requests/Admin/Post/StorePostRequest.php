<?php

namespace App\Http\Requests\Admin\Post;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'post_name'=> ['required', 'unique:posts,name'], 
            'post_category_id'=> ['required'], 
            'post_content'=> ['required'], 
            'post_thumbnail'=> ['required', 'image', 'mimes:jpg,bmp,png'], 
        ];
    }
}
