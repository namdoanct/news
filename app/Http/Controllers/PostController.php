<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostSearchRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(PostSearchRequest $request)
    {
        $data = [];

        $keyword = $request->keyword;
        $categoryId = $request->category_id;
        $posts = Post::with('category');

        // search với keyword
        if (!empty($keyword)) {
            $posts->where('name', 'like', '%' . $keyword . '%');
        }

        // Search với category_id
        if (!empty($categoryId)) {
            $posts->where('category_id', $categoryId);
        }

            $data['posts'] = $posts->select(['id', 'category_id', 'name', 'thumbnail','content'])->paginate(12);

        return view('posts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        $data['categories'] = $categories;

 
        return view('posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];
        $post = Post::findOrFail($id);
        $data['post'] = $post;
 
 
        return view('posts.show', $data);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
