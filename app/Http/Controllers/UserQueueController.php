<?php

namespace App\Http\Controllers;

use App\Imports\QueuedImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ImportUser;

class UserQueueController extends Controller
{
    public function import_with_queue() {
        Excel::import(
            new QueuedImport,
            'data/data.csv'
        );

        // return redirect('/')->with('success', 'Users Imported Successfully!');

        return view('importFileQueue');
    }

    public function import_queue(Request $request){
        Excel::import(new ImportUser, $request->file('file')->store('files'));
        return redirect()->back();
    }
}
