<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StorePostRequest;
use App\Http\Requests\Api\UpdatePostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $posts = Post::select(['id', 'category_id', 'name', 'thumbnail', 'created_at', 'updated_at'])
            ->with('category')
            ->paginate(12);
        return PostResource::collection($posts);

        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        $postInsert = [
            'name' => $request->name,
            'category_id' => $request->category_id,
            'content' => $request->content
        ];

        // uploadthumbnail 
        $extension = $request->thumbnail->extension();
        $fileName = 'thumbnail_post_' . time() . '.' . $extension;
        $thumbnailPath = $request->file('thumbnail')->storeAs(
            'posts',
            $fileName,
            ['disk' => 'public']
        );
        $postInsert['thumbnail'] = $thumbnailPath;

        try {
            // Insert into data to table category (successful)
            $post = Post::create($postInsert);

            return new PostResource($post);
        } catch (\Exception $ex) {
            // Insert into data to table category (fail)
            Log::error($ex->getMessage());
            return response()->json(['erorr' => 'tao bai viet that bai'], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $posts = Post::findOrFail($id);
        return new PostResource($posts);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        dd($request->all());
        try {
            $post = Post::findOrFail($id);

            $postThumbnailOld = $post->thumbnail;

            // Set value for field name
            $post->name = $request->name;
            $post->category_id = $request->category_id;
            $post->content = $request->content;

            // check neu có gửi hình ảnh lên thì lưu vào trong folder storage
            if ($request->thumbnail) {
                $extension = $request->thumbnail->extension();
                $fileName = 'thumbnail_post_' . time() . '.' . $extension;
                $thumbnailPath = $request->file('post_thumbnail')->storeAs(
                    'posts',
                    $fileName,
                    ['disk' => 'public']
                );
                $post->thumbnail = $thumbnailPath;
            }

            $post->save();

            // xóa hình cũ sau khi upload thành công và lưu vào database thành công
            if ($request->thumbnail && $postThumbnailOld) {
                Storage::disk('public')->delete($postThumbnailOld);
            }

            return new PostResource($post);
        } catch (\Exception $ex) {
            // Have error so will show error message
            return response()->json(['erorr' => 'tao bai viet that bai'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $posts = Post::findOrFail($id);
        $posts->delete();
        return response()->json(['message' => 'xoa thanh cong']);
    }
}
