<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryProductResource;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $isShowProduct = request('is_show_product', false);
        $categories = CategoryProduct::latest()->get();
        // Get Data of Category
        if ($isShowProduct) {
            $categories
                ->each(function ($category) {
                    $category->load('limitProducts.category');
                });
        }

        return CategoryProductResource::collection($categories);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }




    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    // public function show(CategoryProduct $categoryProduct)
    // {
    //     $products = CategoryProduct::findOrFail($categoryProduct);
    //     return new CategoryProductResource($categoryProduct);
    // }

    public function show(string $id)
    {
        $products = CategoryProduct::findOrFail($id);
        return new CategoryProductResource($products);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
