<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SearchProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(SearchProductRequest $request)
    {
        // $products = Product::select(['id', 'name', 'thumbnail', 'quantity', 'price', 'created_at', 'updated_at'])
        //     ->paginate(12);
        // return ProductResource::collection($products);


        

        $keyword = $request->keyword;
        $categoryId = $request->category_id;
        $products = Product::with('category');

        // search với keyword
        if (!empty($keyword)) {
            $products->where('name', 'like', '%' . $keyword . '%');
        }

        // Search với category_id
        if (!empty($categoryId)) {
            $products->where('category_id', $categoryId);
        }

        $products = $products->select(['id', 'name', 'thumbnail', 'quantity', 'price', 'created_at', 'updated_at'])->paginate(12);

        return ProductResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    public function checkInventory(Product $product, Request $request)
    {
        $quantity = $request->quantity;

        // Check Inventory
        if ($quantity > $product->quantity) {
            return response()->json([
                'error' => 'The quantity entered has exceeded the inventory.',
            ], 400);
        }

        // Return no-content
        return response()->json([], 204);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::findOrFail($id);
        // dd($product->category);
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
