<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Jobs\OrderMailJob;
use App\Mail\OrderMail;
use App\Mail\WelcomeEmail;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orders = Order::with('orderDetails')
            ->with('user')
            // ->where('user_id', Auth::id())
            ->paginate(12);
        return OrderResource::collection($orders);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        //create data for Order
        $dataSaveOrder = [
            'user_id' => auth()->id() ?? null,
            'name' => $request->name,
            'address' => $request->address,
            'email' => $request->email,
            'phone' => $request->phone,
        ];

        // create data for OrderDetail
        $dataSaveOrderDetail = [];
        foreach ($request->order_details as $item) {
            $product = Product::findOrFail($item['product_id']);
            $dataSaveOrderDetail[] = new OrderDetail([
                'product_id' => $item['product_id'],
                'quantity' => $item['quantity'],
                'price' => $product->price,
            ]);
        }
        try {
            // save Order
            $order = Order::create($dataSaveOrder);

            $order->orderDetails()->saveMany($dataSaveOrderDetail);
            DB::commit();

            OrderMailJob::dispatch($request->email);
            

            return new OrderResource($order);
            // return response()->json([
            //     'success' => 'Create Order succesful.'
            // ]);
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => 'Create Order fail.'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $orders = Order::findOrFail($id);
        return new OrderResource($orders);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
