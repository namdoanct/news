<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\HandleLoginFormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function handleLoginForm(HandleLoginFormRequest $request)
    {
        //set the remember me cookie if the user check the box
        $remember_me = $request->has('remember') ? true : false;
        if ($remember_me) {
            $rememberTokenCookieKey = Auth::getRecallerName();
            Cookie::queue($rememberTokenCookieKey, Cookie::get($rememberTokenCookieKey), 60 * 24 * 7);
        }

        //attempt to log the user in
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember_me)) {
            //if unsuccessful, then redirect back to the login with the form data
            return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors(['email' => 'Email or password not correct']);
        }
        //if successful, then redirect to their intended location
        return redirect()->intended(route('home'));

    }

    public function logout()
    {
        Auth::logout();
        Session::put('success', 'You are logout successfully');
        return redirect(route('login'));
    }
}






