<?php

namespace App\Http\Controllers;

use App\Mail\TestSendMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TestSendMailController extends Controller
{
    public function send(){
        $user = User::firstOrFail();
        Mail::to($user)->send(new TestSendMail());
    }
}
