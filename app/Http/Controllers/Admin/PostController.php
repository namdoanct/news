<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\StorePostRequest;
use App\Http\Requests\Admin\Post\UpdatePostRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];


        $posts = Post::with('category')->orderBy('created_at', 'DESC')->paginate(11);
        $data['posts'] = $posts;
        return view('backend.posts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        $data['categories'] = $categories;

        return view('backend.posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePostRequest $request)
    {
        // dd(Storage::url('posts/thumbnail_post_1681302026.jpg'));
        $postInsert = [
            'name' => $request->post_name,
            'category_id' => $request->post_category_id,
            // 'thumbnail' => $request -> post_thumbnail,
            'content' => $request->post_content
        ];

        // uploadthumbnail 
        $extension = $request->post_thumbnail->extension();
        $fileName = 'thumbnail_post_' . time() . '.' . $extension;
        $thumbnailPath = $request->file('post_thumbnail')->storeAs(
            'posts',
            $fileName,
            ['disk' => 'public']
        );
        $postInsert['thumbnail'] = $thumbnailPath;

        try {
            // Insert into data to table category (successful)
            Post::create($postInsert);

            return redirect()->route('admin.posts.index')
                ->with('sucess', trans('message.store_post_success'));
        } catch (\Exception $ex) {
            // Insert into data to table category (fail)
            Log::error($ex->getMessage());
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = [];
        $post = Post::findOrFail($id);
        $data['post'] = $post;


        return view('backend.posts.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = [];

        $categories = Category::pluck('name', 'id');
        $data['categories'] = $categories;

        $post = Post::findOrFail($id);
        $data['post'] = $post;

        return view('backend.posts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePostRequest $request, string $id)
    {
        try {
            $post = Post::findOrFail($id);

            $postThumbnailOld = $post->thumbnail;

            // Set value for field name
            $post->name = $request->post_name;
            $post->category_id = $request->post_category_id;
            $post->content = $request->post_content;

            // check neu có gửi hình ảnh lên thì lưu vào trong folder storage
            if ($request->post_thumbnail) {
                $extension = $request->post_thumbnail->extension();
                $fileName = 'thumbnail_post_' . time() . '.' . $extension;
                $thumbnailPath = $request->file('post_thumbnail')->storeAs(
                    'posts',
                    $fileName,
                    ['disk' => 'public']
                );
                $post->thumbnail = $thumbnailPath;
            }

            $post->save();

            // xóa hình cũ sau khi upload thành công và lưu vào database thành công
            if ($request->post_thumbnail && $postThumbnailOld) {
                Storage::disk('public')->delete($postThumbnailOld);
            }

            return redirect()->route('admin.posts.index')
                ->with('success', trans('Thành Công'));
        } catch (\Exception $ex) {
            // Have error so will show error message
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $post = Post::findOrFail($id);
            $postThumbnail = $post->thumbnail;
            $post->delete();

            // xóa hình ảnh sau khi thực hiên xong thao tác 
            if ($postThumbnail) {
                Storage::disk('public')->delete($postThumbnail);
            }

            return redirect()->route('admin.posts.index')
                ->with('success', trans('Thành Công'));
        } catch (\Exception $ex) {
            // Have error so will show error message
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }
    
    public function saveFeature($id){
        $post=Post::findOrFail($id);
        $isFeature=$post->is_feature;
        if($isFeature){
            $post->is_feature=false;
        }else{
            $post->is_feature=true;
        }
        $post->save();
        return response()->json(['message'=>'success']);
    }
}
