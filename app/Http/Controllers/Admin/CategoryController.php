<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Models\Category;
use Encore\Admin\Form\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    private $category;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $categories = Category::withCount('posts')->get();
        $data['categories'] = $categories;

        return view('backend.categories.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];


        return view('backend.categories.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategoryRequest $request)
    {
        // insert to DB
        $categoryInsert = [
            'name' => $request->category_name
        ];

        try {
            // Insert into data to table category (successful)
            Category::create($categoryInsert);

            return redirect()->route('admin.category.index')
                ->with('sucess', trans('message.store_category_success'));
        } catch (\Exception $ex) {
            // Insert into data to table category (fail)
            Log::error($ex->getMessage());
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = [];
        $category = Category::findOrFail($id);
        $data['category'] = $category;

        return view('backend.categories.edit', $data);  
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            // Create $category
            $category = Category::findOrFail($id);

            // Set value for field name
            $category->name = $request->category_name;

            $category->save();

            return redirect()->route('admin.category.index')
                ->with('success', trans('Thành Công'));
        } catch (\Exception $ex) {
            // Have error so will show error message
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Category::findOrFail($id);

        /**
         * Check Login Before Delete Category ID
         *
         * @check          CategoryID have exist into table products
         * @@relation_key: products.category_id = categories.id
         *
         * @if   ($count > 0) then show error message (because Category ID have exist into table products)
         * @else then can delete this category (because Category ID dont exist into table products)
         */
        $count = Category::whereDoesntHave('posts')
            ->exists();

        try {
            if ($count) {
                /**
                 * @step1 FindOrFail to check Category ID have exist into Database
                 * @step2 Delete Record
                 */
                $category->delete();

                return redirect()->route('admin.category.index')
                    ->with('success', trans('message.delete_category_success'));
            }

            // if: true then show error message
            return redirect()->route('admin.category.index')
                ->with('error', trans('message.delete_category_fail'));
            

        } catch (\Exception $ex) {
            // Have error so will show error message
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }
}
