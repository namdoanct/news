<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    // /*
    // |--------------------------------------------------------------------------
    // | Login Controller
    // |--------------------------------------------------------------------------
    // |
    // | This controller handles authenticating users for the application and
    // | redirecting them to your home screen. The controller uses a trait
    // | to conveniently provide its functionality to your applications.
    // |
    // */

    // use AuthenticatesUsers;

    // /**
    //  * Where to redirect users after login.
    //  *
    //  * @var string
    //  */
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest:admin', ['except' => 'logout']);
    }

    /**
     * Get Login function
     *
     * @return void
     */
    public function getLogin()
    {
        return view('backend.auth.login');
    }

    /**
     * Show the application loginprocess.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        // Validate Data Input
        $validated = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Check Account
        if (auth()->guard('admin')->attempt($validated, $request->input('remember'))) {
            $user = auth()->guard('admin')->user();
            
            return redirect()->route('admin.dashboard');
        }
        
        return back()->with('error', 'your username and password are wrong.')
            ->withInput();
    }

    /**
     * Show the application logout.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        // $request->session()->invalidate();

        // $request->session()->regenerateToken();

        // auth()->guard('admin')->logout();
        // \Session::flush();
        Session::put('success', 'You are logout successfully');
        return redirect(route('admin.login'));
    }
}
