<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Post;

class DashboardController extends Controller
{
    public function index()
    {
        $data = [];

        $countCategory = Category::count();
        $countPost = Post::count();
        $countComment = Comment::count();

        // set data for view
        $data['countCategory'] = $countCategory;
        $data['countPost'] = $countPost;
        $data['countComment'] = $countComment;

        return view('backend.home', $data);
    }
}
