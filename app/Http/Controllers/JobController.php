<?php

namespace App\Http\Controllers;

use App\Jobs\SendWelcomeEmail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index()
    {
        // gửi mauil sau 3 giây
        $emailJob = (new SendWelcomeEmail())->delay(Carbon::now()->addSeconds(3));
        dispatch($emailJob);
        // dd('Mail Send Successfully');

    }
}
