<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request, $id)
    {
        $data = [];
        $keyword = $request->keyword;
        if (!empty($keyword)) {
            $posts = Post::with('category')
                ->where('category_id', $id)
                ->where('name', 'like', '%' . $keyword . '%')
                ->paginate(12);
        } else {
            $posts = Post::with('category')
                ->where('category_id', $id)
                ->paginate(12);
        }

        $data['posts'] = $posts;
        return view('categories.index', $data);
    }
}
