<?php

namespace App\Http\Controllers;

use App\Http\Requests\HomeSearchRequest;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(HomeSearchRequest $request)
    {
        $data = [];
        $keyword = $request->keyword;
        $posts = Post::with('category');

        if (!empty($keyword)) {
            $posts->where('name', 'like', '%' . $keyword . '%');
        }
        $data['posts'] = $posts->select(['id', 'category_id', 'name', 'thumbnail', 'content'])->paginate(13);
        return view('welcome', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
