<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscribePostRequest;
use App\Models\SubscribePost;
use Exception;
use Illuminate\Http\Request;

class SubscribePostController extends Controller
{
    public function store(SubscribePostRequest $request){
        try {
            SubscribePost::create([
                'email'=> $request->subscribe_email,
            ]);
            return response()->json([
                'message' => 'ok',
            ]);
        } catch(Exception $e) { 
            return response()->json([
                'message' => $e->getMessage()
            ]);
          }
        
    }
}

