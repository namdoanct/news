<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class CheckAdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Check login
        if (!Auth::guard('admin')->check() && !in_array(
            Route::currentRouteName(),
            ['admin.login', 'admin.login.handle']
        )) {
            return redirect()->route('admin.login');
        }

        // if (Auth::guard('admin')->check()) {
        //     return redirect()->route('admin.dashboard');
        // }

        // Check login:
        if (Auth::guard('admin')->check() && in_array(
            Route::currentRouteName(),
            ['admin.login', 'admin.login.handle']
        )) {
            return redirect()->route('admin.dashboard');
        }

        return $next($request);
    }
}
