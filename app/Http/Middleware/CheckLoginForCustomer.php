<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class CheckLoginForCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
// truong hop 1: chua login va url dang truy cap khong phai la login thi se di chuyen den trang login
        if (!Auth::check() && !in_array(
            Route::currentRouteName(),
            ['login', 'login.handle']
        )) {
            return redirect()->route('login');
        }

        // TH2: da login roi nhung van co tinh truy cap url login thi se di chuyen ve trang chu
        if (Auth::check() && in_array(
            Route::currentRouteName(),
            ['login', 'login.handle']
        )) {
            return redirect()->route('home');
        }

        return $next($request);

    }
}
