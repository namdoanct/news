<?php

namespace App\Models;

use Carbon\Factory;
use Database\Factories\CommentFactory;
use Faker\Factory as FakerFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;


    protected $fillable = [
        'post_id',
        'user_id',
        'content',
        
    ];
 
       /**
    * Get the category that owns the post.
    */
   public function comment()
   {
       // case 1: viết ngắn gọn
       return $this->belongsTo(Comment::class);


       // case 2: viết đầy đủ 
       // return $this->belongsTo(Category::class, 'category_id', 'id');
   }

       /**
     * Create a new factory instance for the model.
     */

}
