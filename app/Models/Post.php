<?php

namespace App\Models;

use Database\Factories\PostFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory; 

    protected $fillable = [
        'category_id',
        'name',
        'thumbnail',
        'content',
        'is_feature',
    ];
 
       /**
    * Get the category that owns the post.
    */
   public function category()
   {
       // case 1: viết ngắn gọn
       return $this->belongsTo(Category::class);

       // mối quan hệ giữa post và category là gì
        // post thuộc về category
        // category có nhiều post
        // post có 1 category



       // case 2: viết đầy đủ 
       // return $this->belongsTo(Category::class, 'category_id', 'id');
   }

       /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return PostFactory::new();
    }

}
