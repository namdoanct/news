<?php

namespace App\Models;

use Database\Factories\CategoryProductFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class CategoryProduct extends Model
{
    use HasFactory;

    
    protected $fillable = [
        'name',
        'slug',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function limitProducts ()
    {
        return $this->products()->take(4);
    }

    /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return CategoryProductFactory::new();
    }

    protected static function booted()
    {
        static::creating(function ($query){
            $query->slug = Str::slug($query->name);
        });
    }
}
