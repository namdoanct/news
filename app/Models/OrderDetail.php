<?php

namespace App\Models;

use Database\Factories\OrderDetailFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
    ];
 
       /**
    * Get the category that owns the post.
    */
   public function order()
   {
       return $this->belongsTo(Order::class);
   }

       /**
     * Create a new factory instance for the model.
     */
    protected static function newFactory(): Factory
    {
        return OrderDetailFactory::new();
    }


}
