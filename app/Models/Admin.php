<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use HasFactory;

    use SoftDeletes;

    protected $table = 'admins';
    
    protected $guarded = [];

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The roles that belong to the admin.
     *
     * @Reference: https://laravel.com/docs/8.x/eloquent-relationships#many-to-many-defining-the-inverse-of-the-relationship
     *
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Get the admin's role name.
     *
     * @return string
     */
    public function getRoleNames()
    {
        $roleNames = $this->roles->pluck('name')->toArray();
        return implode(', ', $roleNames);
    }

    /**
     * Get the admin's role id.
     *
     * @return string
     */
    public function getRoleIds()
    {
        $roleIds = $this->roles->pluck('id')->toArray();
        return $roleIds;
    }

    /**
     * Get the admin's display_created_at.
     *
     * @reference: https://laravel.com/docs/8.x/eloquent-mutators#defining-an-accessor
     *
     * @return string
     */
    public function getDisplayCreatedAtAttribute(): string
    {
        return $this->created_at->format('d/m/Y H:i:s');
    }

    /**
     * Check Is Admin function
     *
     * @return boolean
     */


    /**
     * Check Is Employee function
     *
     * @return boolean
     */


}
