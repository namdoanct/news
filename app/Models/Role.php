<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $table = 'roles';

    protected $fillable = [
        'name',
    ];

    /**
     * The admins that belong to the role.
     *
     * @Reference: https://laravel.com/docs/8.x/eloquent-relationships#many-to-many-defining-the-inverse-of-the-relationship
     *
     * @return mixed
     */
    public function admins()
    {
        return $this->belongsToMany(Admin::class);
    }
}
