<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrap();

        $featurePosts = Post::where('is_feature',1)->take(10)->get();
        View::share('featurePosts', $featurePosts);

        $shareCategories = Category::all();
        View::share('shareCategories', $shareCategories);
  
    }


}
