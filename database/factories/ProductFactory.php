<?php

namespace Database\Factories;

use App\Models\CategoryProduct;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'content' => fake()->realText(),
            'thumbnail' =>  'https://picsum.photos/200/300',
            'quantity' => $this->faker->numberBetween(10,20),
            'price' => $this->faker->numberBetween(50000,100000),
            'category_product_id' => CategoryProduct::factory(),
        ];
    }
}
