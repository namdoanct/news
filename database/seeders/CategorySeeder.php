<?php

namespace Database\Seeders;

use App\Models\Category;
use Exception;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories= [
            'Bao_tuoi_tre',
            'Bao_24h',
            'Bao_lao_dong'
        ];

        foreach ($categories as $categoryName) {
            try {
                // Insert into table categories
                Category::create([
                    'name' => $categoryName,
                ]);
           
                echo "Inserted ($categoryName): Passed";
                echo PHP_EOL; // new line
            } catch (Exception $exception) {
                echo 'Insert '. $categoryName .': Failed - Error Message: ' . $exception->getMessage();
                echo PHP_EOL; // new line
            }
        }
    }
 }
 

