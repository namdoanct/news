<?php

namespace Database\Seeders;

use App\Models\CategoryProduct;
use App\Models\Comment;
use App\Models\Post;
use Database\Factories\CommentFactory;
use Exception;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */

     public function run(): void
     {
         // Data for Category (Level 1)
         $categories = [
             'Ngao - Sò - Ốc',
             'Bào ngư',
             'Tôm',
             'Mực',
             'Cá',
             'Cua ghẹ',
             'Sứa',
             'Hải sản đóng hộp',
         ];
 
         foreach ($categories as $categoryName) {
             try {
                 // Insert into table categories
                 CategoryProduct::create([
                     'name' => $categoryName,
                 ]);
 
                 echo "Inserted ($categoryName): Passed";
                 echo PHP_EOL; // new line
             } catch (Exception $exception) {
                 echo 'Insert ' . $categoryName . ': Failed - Error Message: ' . $exception->getMessage();
                 echo PHP_EOL; // new line
             }
         }
     }

    // public function run(): void
    // {
    //     Category::inRandomOrder()
    //             ->limit(5)
    //             ->get();
    // }
}
