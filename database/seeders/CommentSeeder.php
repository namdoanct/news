<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Post;
use Carbon\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Nette\Utils\Random;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $posts = Post::get();

        foreach ($posts as $post) {
            $postId = $post->id;
            // tao ra nhieu comment 
            Comment::factory()
                ->count(3)
                ->state(['post_id'=> $postId])
                ->create();
        }
    }
}
