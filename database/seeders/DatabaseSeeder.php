<?php

namespace Database\Seeders;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\User;
use Illuminate\Support\Str;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(1)->create();

        $this->call(AdminSeeder::class);

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        // tao ra 10 user
        // moi ong user co 5 order
        // trong moi on thi co 2 san phan

        // User::factory()
        //     ->has(
        //         Order::factory()
        //             ->has(
        //                 OrderDetail::factory()
        //                 ->count(2)
        //             )
        //             ->count(5)
        //     )
        // ->count(10)
        // ->create();


        //  TH1: use Seeder no factory 
        $this->call(CategorySeeder::class);
        $this->call(PostSeeder::class);

        // TH2: use factory 
        // $this->call(CategoryFactory::class);


        // $this->call(CommentSeeder::class);

            $this->call(CategoryProductSeeder::class);

        $this->call(ProductSeeder::class);
    }
}
