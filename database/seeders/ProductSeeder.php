<?php

namespace Database\Seeders;


use App\Models\CategoryProduct;
use App\Models\Product;
use Exception;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        echo json_encode('[Begin] Extract File ZIP (product_upload.zip)');
        echo PHP_EOL; // new line

        // Check and remove folder exists
        if (public_path('product_upload')) {
            File::deleteDirectory(public_path('product_upload'));
        }

        Storage::disk('public')->deleteDirectory('product_upload');


        $zip = new ZipArchive;
        $isExtractOK = false;
        if ($zip->open(public_path('product_upload.zip')) === TRUE) {
            $zip->extractTo(public_path());
            $zip->extractTo(storage_path('app/public'));
            $zip->close();
            $isExtractOK = true;
            echo 'Extract File ZIP Successful.';
            echo PHP_EOL; // new line

            // Check and remove folder exists
            if (public_path('product_upload')) {
                File::deleteDirectory(public_path('product_upload'));
            }
        }

        echo json_encode('[End] Extract File ZIP (product_upload.zip)');
        echo PHP_EOL; // new line

        // Check $isExtractOK
        if (!$isExtractOK) {
            echo 'Extract File ZIP Fail';
            echo PHP_EOL; // new line
            return;
        }

        echo json_encode('[Begin] Insert Data for module Product');
        echo PHP_EOL; // new line

        // Include Data
        include 'data_fakes/products/ngao_so_oc.php';
        include 'data_fakes/products/bao_ngu.php';
        include 'data_fakes/products/tom.php';
        include 'data_fakes/products/muc.php';
        include 'data_fakes/products/ca.php';
        include 'data_fakes/products/cua_ghe.php';
        include 'data_fakes/products/sua.php';
        include 'data_fakes/products/hai_san_dong_hop.php';

        // Prepare Data to Insert into table products
        $dataInserts = [
            $ngaoSoOc,
            $baoNgu,
            $tom,
            $muc,
            $ca,
            $cuaGhe,
            $sua,
            $haiSanDongHop,
        ];

        // Quantity
        include 'data_fakes/quantities.php';

        // Price
        include 'data_fakes/prices.php';

        // check data available
        if (!empty($dataInserts)) {
            
            foreach ($dataInserts as $value) {
                // define variable and set value for them
                $categorySlug = $value['category_slug'];
                $products = $value['products'];

                // Get Category
                $category = CategoryProduct::where('slug', $categorySlug)
                    ->first();
                // dd($category);
                // Check $category
                if (!empty($category) && !empty($products)) {
                    foreach ($products as $product) {
                        Log::info('aaaa: ' . $categorySlug);
                        try {
                            // Save data into table products
                            $dataProductSave = Product::create([
                                'name' => $product['name'],
                                'thumbnail' => $product['thumbnail'],
                                'content' => $product['content'],
                                'quantity' => $quantities[array_rand($quantities)],
                                'price' => $prices[array_rand($prices)],
                                'category_product_id' => $category->id,
                            ]);

                            Log::error(json_encode($dataProductSave));

                            // // Save data into table product_images
                            // if (!empty($product['images'])) {
                            //     $productImages = $product['images'];
                            //     foreach ($productImages as $imageUrl) {
                            //         $dataProductSave->productImages()->create([
                            //             'url' => $imageUrl,
                            //         ]);
                            //     }
                            // }

                            echo 'Inserted ' . $product['name'] . ': Passed';
                            echo 'Inserted product_images: Passed';
                            echo PHP_EOL; // new line
                        } catch (Exception $exception) {
                            echo 'Insert ' . $product['name'] . ': Failed - Error Message: ' . $exception->getMessage();
                            echo PHP_EOL; // new line
                        }
                    }
                }
            }
        }

        echo json_encode('[End] Insert Data for module Product');
        echo PHP_EOL; // new line
    }

    // public function run(): void
    // {
    //     CategoryProduct::factory()
    //         ->hasProducts(12)
    //         ->count(3)
    //         ->create();
    // }
}
