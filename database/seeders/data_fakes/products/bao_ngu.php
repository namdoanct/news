<?php

$baoNgu = [
    'category_slug' => 'bao-ngu', // Bào Ngư
    'products' => [    
        [
            'name' => 'Cháo bào ngư',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-1.jpg',
            'content' => 'Cháo bào ngư có giá trị dinh dưỡng cao, phù hợp cho cả người khỏe mạnh lẫn người đang bị ốm. Vị ngọt đậm đà từ nước xương hầm kết hợp với vị ngọt dai của của thịt bào ngư cùng nguyên liệu rau củ bổ sung như cà rốt, húp miếng cháo bào ngư bạn sẽ cảm thấy khác hẳn so với các loại cháo khác.',
            'images' => [
                'eimages/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
        [
            'name' => 'Cơm bào ngư',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-2.jpg',
            'content' => 'Bào ngư có thể làm ra món cơm bào ngư rất lạ miệng. Bào ngư được thái lát và chế biến kèm với nước sốt đặc trưng, rưới lên cơm kèm theo một số loại rau luộc tùy theo sở thích của bạn.',
            'images' => [
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
        [
            'name' => 'Mì bào ngư xào',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-3.jpg',
            'content' => 'Không chỉ làm món cơm mà bào ngư cũng trở thành nguyên liệu hải sản để nấu chung với mì. Sợi mì vàng óng kết hợp với những lát bào ngư (hoặc để nguyên con) nhìn trông hấp dẫn mà bạn khó lòng cưỡng lại.',
            'images' => [
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
        [
            'name' => 'Bào ngư sốt dầu hào',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-4.jpg',
            'content' => 'Bào ngư có thể được xào với nấm đông cô (nấm hương), cải bó xôi hay bất kì loại nấm, rau củ nào mà bạn thích kết hợp với sốt dầu hào đậm đà, hấp dẫn. Vị ngọt dai của bào ngư kèm theo nước sốt màu nâu hấp dẫn, mặn ngọt kích thích cả bao tử của bạn trong suốt bữa ăn.',
            'images' => [
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
        [
            'name' => 'Bào ngư hấp',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-5.jpg',
            'content' => 'Bào ngư hấp có thể giúp bạn cảm nhận trọn vẹn hương vị của loại hải sản này mà không bị ảnh hưởng các nguyên liệu khác. Bạn có thể ăn trực tiếp, chấm cùng với nước tương hay bất kì nước sốt nào.',
            'images' => [
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
        [
            'name' => 'Súp bào ngư',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-6.jpg',
            'content' => 'Bào ngư còn được hầm chung với thuốc bắc, nấm đông cô (nấm hương), sườn heo, bí đao,... để làm ra những món súp có hương vị riêng. Đây là món ăn bổ dưỡng trong những nhà hàng sang trọng hoặc những gia đình có điều kiện, chú trọng đến sức khỏe vì giá trị dinh dưỡng cao của loại hải sản này.',
            'images' => [
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
        [
            'name' => 'Lẩu bào ngư',
            'thumbnail' => '/storage/product_upload/bao-ngu/thumbnail-bao-ngu-7.jpg',
            'content' => 'Thay đổi khẩu vị với món lẩu bào ngư cho bữa ăn cuối tuần của gia đình bạn. Các nguyên liệu hải sản tươi ngọt kết hợp với nước dùng thanh thanh, nóng hổi, nghe thôi đã muốn bắt tay vào làm thử đấy!',
            'images' => [
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-1.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-2.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-3.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-4.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-5.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-6.jpg',
                '/storage/product_upload/bao-ngu/images/image-bao-ngu-7.jpg',
            ],
        ],
    ],
];