$(document).ready(function () {
    // Format Datetime
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'YYYY-MM-DD'
        },
        "autoUpdateInput": false
    });

    /**
     * Format Price
     * 
     * reference: https://price-format.github.io/Jquery-Price-Format/
     */
    $('.js-price').priceFormat({
        prefix: '',
        centsSeparator: '.',
        thousandsSeparator: ',',
        clearOnEmpty: true
    });

    // Select2
    $('.category-select2').select2({
        placeholder: "Select a Category"
    });

    // Catch event onChange
    $('.js-price').on('change', function () {
        var val = $(this).val();

        console.log('val', val);

        if (val == 'VNĐ 0.00') {
            console.log('vao day ko')
            $(this).val('0000000');
        }
    });
});

/**
 * 
 * @param {*} that 
 */
function submitFilterProduct(that) {
    // Get value of per_page
    var perPage = $(that).val();
    
    // Set value for input hidden
    $('#per-page').val(perPage);

    // Trigger submit form
    $('#frm-product-search').submit();
}
