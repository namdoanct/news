$(document).ready(function () {
    /**
     * Set Datetimepicker for beginDate
     * 
     * @Format YYYY-MM-DD hh:mm:ss
     * 
     * @reference: https://www.daterangepicker.com/#config
     */
     $('.js-expiration-date').daterangepicker({
        "timePicker": true,
        "timePicker24Hour": true,
        "timePickerSeconds": true,
        "autoApply": true,
        "startDate": beginDate,
        "endDate": endDate,
        "minDate": minDate,
        "maxDate": maxDate,
        locale: {
            format: 'DD/MM/YYYY HH:mm:ss'
        }
    }, function(start, end, label) {
        console.log('start', start);
        console.log('end', end);

        console.log('New date range selected: ' + start.format('DD/MM/YYYY HH:mm:ss') + ' to ' + end.format('DD/MM/YYYY HH:mm:ss') + ' (predefined range: ' + label + ')');
    });

     // Catch event onChange for promotion-type
    $('.promotion-type').on('change', function () {
        var type = $(this).val();
        var discount = $('.promotion-discount').val();

        console.log('type', type);

        if (type == 'percent') {
            // Set Attribute for max
            $('.promotion-discount').attr('max', 99);

            // Set Attribute for placeholder
            $('.promotion-discount').attr('placeholder', '0 ~ 99');

            // Check and Set Default Value
            if (discount < 1 || discount > 99) {
                $('.promotion-discount').val('');
            }
        } else {
            // Set Attribute for max
            $('.promotion-discount').attr('max', 999999999);

            // Set Attribute for placeholder
            $('.promotion-discount').attr('placeholder', '0 ~ 999999999');

            // Check and Set Default Value
            if (discount < 1 || discount > 999999999) {
                $('.promotion-discount').val('');
            }
        }
    });

    // Catch event onChange for promotion-discount
    $('.promotion-discount').on('change', function () {
        var discount = $(this).val();
        var type = $('.promotion-type:checked').val();

        console.log('type', type);
        console.log('discount', discount);

        if (type == 'percent') {
            if (discount < 1 || discount > 99) {
                // Show Error Message
                displayErrorMessage('Con số nhập vào Discount không hợp lệ. Con số hợp lệ nằm trong khoảng [1 ~ 99]');

                // Set Default NULL
                $('.promotion-discount').val('');
            }
        } else {
            if (discount < 1 || discount > 999999999) {
                // Show Error Message
                displayErrorMessage('Con số nhập vào Discount không hợp lệ. Con số hợp lệ nằm trong khoảng [1 ~ 999999999]');

                // Set Default NULL
                $('.promotion-discount').val('');
            }
        }
    });

    // Init Select2
    settingPromotionForProduct();
});



function settingPromotionForProduct() {
    // Ajax Get Product List
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: BASE_URL + '/promotion/ajax-get-product-list',
        type: 'GET',
        success: function (data) {
            console.log(data);

            $('.js-select2').select2({data: data['products']});

            $('.js-select2').val(data['product_ids'])
                .trigger('change');
        },
        error: function (err) {
            console.log(err);

            // // Show Error Messsage
            // displayErrorMessage(err.responseJSON.message);
        },
        dataType: 'json'
    });
}
