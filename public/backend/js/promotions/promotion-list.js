$(document).ready(function () {
    // Format Datetime
    $('.datepicker').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });

    // Init Select2
    $('.js-select2').select2();
});

/**
 * 
 * @param {*} that 
 */
function submitFilterPromotion(that) {
    // Get value of per_page
    var perPage = $(that).val();

    // Set value for input hidden
    $('#per-page').val(perPage);

    // Trigger submit form
    $('#frm-promotion-search').submit();
}

/**
 * 
 * @param {*} that 
 * @param {*} url 
 * 
 * @reference
 * @@fill data: https://select2.org/data-sources/arrays
 * @@selected: https://select2.org/programmatic-control/add-select-clear-items#selecting-options
 * 
 */
function modalSettingPromotion(that, url) {
    var promotionId = $(that).closest('tr').data('id');
    $('#js-promotion-id').val(promotionId);

    // Ajax Get Product List
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: url,
        type: 'GET',
        success: function (data) {
            console.log(data);

            $('.js-select2').select2({data: data['products']});

            $('.js-select2').val(data['product_ids'])
                .trigger('change');
        },
        error: function (err) {
            console.log(err)
        },
        dataType: 'json'
    });

    // Display Modal
    $('#modal-setting-promotion').modal();
}

function settingPromotionForProduct() {
    var collect = $('.js-select2').select2('data');

    var productIds = [];

    $.each(collect, function () {
        productIds.push(this.id);
    });

    var promotionId = $('#js-promotion-id').val();


    // Ajax Get Product List
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: BASE_URL + '/promotion/' + promotionId + '/setting',
        type: 'PUT',
        data: {
            'product_ids': productIds
        },
        success: function (data) {
            console.log(data);

            // Show Message Success
            displaySuccessMessage(data.message);

            // Close Modal
            $('#modal-setting-promotion').modal('hide');
        },
        error: function (err) {
            console.log(err);

            // Show Error Messsage
            displayErrorMessage(err.responseJSON.message);

            // Close Modal
            $('#modal-setting-promotion').modal('hide');
        },
        dataType: 'json'
    });
}
