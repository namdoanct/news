// A $( document ).ready() block.
$( document ).ready(function() {
    console.log( "ready!" );

    $('#form-subscribe').on('submit', function(event){
        event.preventDefault();
        $.ajax({
           type:'POST',
           url:$('#form-subscribe').attr('action'),
           data: $('#form-subscribe').serialize(),
           success:function(data) {
              console.log(data);
              alert(data.message)
           },
           error:function(err) {
            console.log(err);
            // alert(err.message)
            var tmp=err.responseJSON.errors;
            alert(tmp.subscribe_email[0])
         },
         dataType:'json'
        });
    });
});


