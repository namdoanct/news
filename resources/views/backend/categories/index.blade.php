@extends('backend.layouts.master')

{{-- set page title --}}
@section('title', __('message.category_list'))

{{-- set breadcrumbName --}}
@section('breadcrumbName', __('Danh sách category'))

{{-- set breadcrumbMenu --}}
@section('breadcrumbMenu', __('message.category_list'))

{{-- import file css (private) --}}
@push('css')
    <link rel="stylesheet" href="/backend/css/categories/category-list.css">
@endpush

@section('content')
    {{-- form search --}}
    @include('backend.categories._search')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif

    {{-- display list category table --}}
    @if($categories->isEmpty())
        <p class="red">{{ __('message.not_found_data') }}</p>
    @else
        <table id="category-list" class="table table-bordered table-hover table-striped">
            <thead class="bg-info">
                <tr>
                    <th class="text-center">#</th>
                    <th> Tên category</th>
                    <th>Số lượng bài viết</th>
                    <th colspan="2">Thao tác</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($categories))
                    @foreach ($categories as $key => $category)
                        <tr>
                            <td class="text-center">{{ $key+1 }}</td>
                            <td>{{ $category->name }}</td>
                            <td><a href="{{ route('admin.posts.index', ['category_id[]' => $category->id]) }}" class="btn btn-link btn-common"><i class="fas fa-search-plus"></i><span class="ml-2">Số lượng bài viết(<span class="font-weight-bold">{{ $category->posts_count }}</span>)</span></a></td>
                            <td><a href="{{ route('admin.category.edit', $category->id) }}" class="btn btn-info btn-common" title="{{ __('message.edit_product') }}"><i class="fas fa-edit"></i> {{ __('Chỉnh sửa') }}</a></td>
                            <td>
                                @if($category->posts_count  == 0)
                                    <form action="{{ route('admin.category.destroy', $category->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('bạn có muốn xóa không ?')"><i class="fas fa-trash-alt"></i> {{ __('Xóa') }}</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    @endif
@endsection
