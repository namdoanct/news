@extends('backend.layouts.master')

{{-- set page title --}}
@section('title', __(''))

{{-- set breadcrumbName --}}
@section('breadcrumbName', __('Danh sách bài viết'))

{{-- set breadcrumbMenu --}}
@section('breadcrumbMenu', __(''))

{{-- import file css (private) --}}
@push('css')
<link rel="stylesheet" href="/backend/css/categories/category-edit.css">
@endpush

@section('content')
{{-- error message --}}
@include('errors.error_message')

<form action="{{ route('admin.posts.update', $post->id) }}" method="post" enctype="multipart/form-data" >
    @csrf
    @method('PUT')

    <div class="form-group mb-3">
        <div class="row">
            <div class="col-md-4">

                <div class="mb-5">
                    <label for="" class="required">{{ __('Tên bài viết') }}</label>
                    <input type="text" name="post_name" class="form-control" value="{{ old('post_name', $post->name) }}">
                    @error('post_name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-5">
                    <label for="" class="required">{{ __('Số thứ tự category') }}</label>
                    <select name="post_category_id" class="form-control">
                        @foreach ($categories as $categoryId => $categoryName)
                        <option value="{{ $categoryId }}" {{ old('post_category_id', $post->category_id) == $categoryId ? 'selected' : ''}}>{{ $categoryName }}</option>
                        @endforeach
                    </select>
                    @error('post_category_id')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-5">
                    <label for="">{{ __('Nhập hình ảnh') }}</label>
                    <img src="{{ Storage::url($post->thumbnail) }}" alt="{{ $post->name }}">
                    <input type="file" name="post_thumbnail" class="form-control">
                    @error('post_thumbnail')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>


                <div class="mb-5">
                    <label for="" class="required">{{ __('Nhập nội dung') }}</label>
                    <textarea type="text" name="post_content" class="form-control">{{ old('post_content', $post->content) }}</textarea>
                    @error('post_content')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

            </div>
        </div>
    </div>

    <p><span class="red">(*)</span> {{ __('Bắt buộc') }}</p>

    <div class="form-group">
        <a href="{{ route('admin.posts.index') }}" class="btn btn-secondary"><i class="fas fa-long-arrow-alt-left"></i> <span class="ml-2">{{ __('Trở về') }}</span></a>
        <button type="submit" class="btn btn-primary"><i class="far fa-edit"></i> <span class="ml-2">{{ __('Cập nhật') }}</span></button>
    </div>
</form>
@endsection