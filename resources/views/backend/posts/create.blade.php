@extends('backend.layouts.master')

{{-- set page title --}}
@section('title', __('tạo bài viết'))

{{-- set breadcrumbName --}}
@section('breadcrumbName', __('Danh sách bài viết'))

{{-- set breadcrumbMenu --}}
@section('breadcrumbMenu', __('tạo bài viết'))

{{-- import file css (private) --}}
@push('css')
<link rel="stylesheet" href="/backend/css/categories/category-create.css">
@endpush

@section('content')
{{-- error message --}}
@include('errors.error_message')

<form action="{{ route('admin.posts.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <div class="mb-5">
                    <label for="" class="required">{{ __('Tên bài viết') }}</label>
                    <input type="text" name="post_name" class="form-control">
                    @error('post_name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-5">
                    <label for="" class="required">{{ __('Số thứ tự category') }}</label>
                    <select name="post_category_id" class="form-control">
                        @foreach ($categories as $categoryId => $categoryName)
                        <option value="{{ $categoryId }}">{{ $categoryName }}</option>
                        @endforeach
                    </select>
                    @error('post_category_id')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="mb-5">
                    <label for="" class="required">{{ __('Nhập hình ảnh') }}</label>
                    <input type="file" name="post_thumbnail" class="form-control">
                    @error('post_thumbnail')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="mb-5">

                    <label for="" class="required">{{ __('Nhập nội dung') }}</label>
                    <input type="text" name="post_content" class="form-control">
                    @error('post_content')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <p><span class="red">(*)</span> {{ __('Bắt buộc') }}</p>

    <div class="form-group">
        <a href="{{ route('admin.posts.index') }}" class="btn btn-secondary"><i class="fas fa-long-arrow-alt-left"></i> <span class="ml-2">{{ __('Trở về') }}</span></a>
        <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> <span class="ml-2">{{ __('Thêm mới') }}</span></button>
    </div>
</form>
@endsection