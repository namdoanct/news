@extends('backend.layouts.master')

{{-- set page title --}}
@section('title', __('message.category_list'))

{{-- set breadcrumbName --}}
@section('breadcrumbName', __('Danh sách bài viết'))

{{-- set breadcrumbMenu --}}
@section('breadcrumbMenu', __('message.category_list'))

{{-- import file css (private) --}}
@push('css')
<link rel="stylesheet" href="/backend/css/categories/category-list.css">
<link rel="stylesheet" href="/css/switch.css">
@endpush

@push('js')
<script>
    function saveFeatureJS(that) {
        var confirmAction = confirm("Are you sure to execute this?");
        if (confirmAction) {
            var URL = $(that).closest('form').attr('action');
            var CSRF = $(that).closest('form').find('input[name="_token"]').val();
            $.ajax({
                type: 'post',
                url: URL,
                data: {
                    _token: CSRF
                },
                success: function(data) {
                    alert('success');
                },
                error: function(err) {
                    alert('error');
                }
            });
        }
    }
</script>
@endpush

@section('content')
{{-- form search --}}
@include('backend.posts._search')

{{-- show message --}}
@if(Session::has('success'))
<p class="text-success">{{ Session::get('success') }}</p>
@endif

{{-- show error message --}}
@if(Session::has('error'))
<p class="text-danger">{{ Session::get('error') }}</p>
@endif

{{-- display list category table --}}
@if($posts->isEmpty())
<p class="red">{{ __('Không tìm thấy dữ liệu') }}</p>
@else
<table id="category-list" class="table table-bordered table-hover table-striped">
    <thead class="bg-info">
        <tr>
            <th class="text-center">#</th>
            <th>Tên bài viết</th>
            <th>Tên category</th>
            <th>Thumbnail</th>
            <th>Bài viết nổi bật</th>
            <th colspan="2">Thao tác</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($posts))
        @foreach ($posts as $key => $post)
        <tr>
            <td class="text-center">{{ $key+1 }}</td>
            <td>{{ $post->name }}</td>
            <td> {{ $post->category->name}} </td>
            <td><img src="{{ Storage::url($post->thumbnail) }}" alt="{{ $post->name }}" class="img-thumbnail"> </td>
            <td>
                <form method="post" action="{{ route('admin.posts.save-feature', $post->id) }}">
                    @csrf
                    <label class="switch">
                        <input type="checkbox" {{ $post->is_feature ? 'checked' : '' }} onclick="saveFeatureJS(this)">
                        <span class="slider round"></span>
                    </label>
                </form>
            </td>
            <td><a href="{{ route('admin.posts.edit', $post->id) }}" class="btn btn-info btn-common" title="{{ __('message.edit_post') }}"><i class="fas fa-edit"></i> {{ __('Chỉnh sửa') }}</a></td>
            <td>
                <form action="{{ route('admin.posts.destroy', $post->id) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('bạn có muốn xóa không ?')"><i class="fas fa-trash-alt"></i> {{ __('Xóa') }}</button>
                </form>
            </td>
        </tr>
        @endforeach
        @endif
    </tbody>
</table>
@endif
@endsection