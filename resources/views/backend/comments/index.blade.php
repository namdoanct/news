@extends('backend.layouts.master')

{{-- set page title --}}
@section('title', __('message.category_list'))

{{-- set breadcrumbName --}}
@section('breadcrumbName', __('Danh sách bài viết'))

{{-- set breadcrumbMenu --}}
@section('breadcrumbMenu', __('message.category_list'))

{{-- import file css (private) --}}
@push('css')
    <link rel="stylesheet" href="/backend/css/categories/category-list.css">
@endpush

@section('content')
    {{-- form search --}}
    @include('backend.comments._search')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif

    {{-- display list category table --}}
    @if($comments->isEmpty())
        <p class="red">{{ __('Không tìm thấy dữ liệu') }}</p>
    @else
        <table id="category-list" class="table table-bordered table-hover table-striped">
            <thead class="bg-info">
                <tr>
                    <th class="text-center">#</th>
                    <th>ID bài viết</th>
                    <th>ID người dùng</th>
                    <th> Nội dung</th>
                    <th colspan="2">Thao tác</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($comments))
                    @foreach ($comments as $key => $comment)
                        <tr>
                            <td class="text-center">{{ $key+1 }}</td>
                            <td>{{ $comment->name }}</td>
                            <td> {{ $comment->category->name}} </td>
                            <td>{{ $comment->content }}</td>
                            <td><a href="{{ route('admin.comments.edit', $comment->id) }}" class="btn btn-info btn-common" title="{{ __('message.edit_comment') }}"><i class="fas fa-edit"></i> {{ __('Chỉnh sửa') }}</a></td>
                            <td>
                                    <form action="{{ route('admin.comments.destroy', $post->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('bạn có muốn xóa không ?')"><i class="fas fa-trash-alt"></i> {{ __('Xóa') }}</button>
                                    </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    @endif
@endsection
