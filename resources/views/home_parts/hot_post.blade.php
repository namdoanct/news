
<div class="d-flex flex-wrap">


    @foreach ($posts as $post)
    <div class="">
        <h5><a href="{{route('posts.show',$post->id)}}"> {{ $post->name }} </a></h5>

        <p><a href="{{route('category-list',$post->category_id)}}"> {{ $post->category->name}} </a></p>
        <h6>{{ $post->content }}</h6>
        <img src="{{$post->thumbnail}}" alt="{{ $post->name }}">
    </div>
    <hr>
    @endforeach
</div>