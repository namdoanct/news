<div class="leftcolumn" id="myNav">
    <h2>Tin tức nổi bật</h2>

    <div class="sidebar-homepage">
        @foreach ($featurePosts as $featurePost)
        <div class="posts_list">
            <p><a href="{{route('posts.show',$featurePost->id)}}"> {{ $featurePost->name }} </a></p>
        </div>
        @endforeach
    </div>
</div>