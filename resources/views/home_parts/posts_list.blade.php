<!-- <div class="d-flex flex-wrap"> -->

@php
$count=0;
@endphp
@foreach ($posts as $key=>$post)
@if($count==0)
<div class="post1">
    <div class="post-home-thumbnail"><a href="{{route('posts.show',$post->id)}}"> <img src="{{$post->thumbnail}}" alt="{{ $post->name }}" > </a></div>
    <div class="posts2">
        <h5 class="post1-name"><a href="{{route('posts.show',$post->id)}}"> {{ $post->name }} </a></h5>

        <p class="postall-content"><a href="{{route('posts.show',$post->id)}}"> {{ $post->content }} </a> </p>
    </div>
</div>
@else
@if($key%4==1)<div class="row mb-3 post-all-detail">@endif
    <div class="row post-alls">
        <div class="col-5">
            <div class="postall-home-thumbnail"><a href="{{route('posts.show',$post->id)}}"> <img src="{{$post->thumbnail}}" alt="{{ $post->name }}" style="height:50%"> </a></div>
        </div>
        <div class="col-7">
            <h5 class="postall-name"><a href="{{route('posts.show',$post->id)}}"> {{ $post->name }} </a></h5>
            <!-- <p><a href="{{route('category-list',$post->category_id)}}"> {{ $post->category->name}} </a></p> -->
            <p class="postall-content"><a href="{{route('posts.show',$post->id)}}"> {{ $post->content }} </a> </p>
        </div>
    </div>
    @if($key%4==0)
</div>@endif
@endif

@php
$count++;
@endphp
@endforeach
<!-- </div> -->

<div>
    {{ $posts->appends(request()->input())->links() }}
</div>