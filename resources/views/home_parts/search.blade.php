@php
$url= route('home');
$currentUrl= url()->current();
if($currentUrl!=$url){
$url=$currentUrl;
}
@endphp

<div class="d-flex flex-nowrap justify-content-between">
    <div class="form-group">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle shadow-none" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                <span class="fa fa-user"></span><span class="text">&nbsp;Tài khoản</span>
            </button>
            <ul class="dropdown-menu dropdown-menu-dark account-info" aria-labelledby="dropdownMenuButton2">
                @if(Auth::check())
                <li><a class="dropdown-item" href="{{ route('profile') }}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;
                        {{Auth::user()->name}}</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li>
                    <form method="post" action="{{route('logout')}}">
                        @csrf
                        <input type="submit" value="logout" class="">
                    </form>
                </li>
                @else
                <li><a class="dropdown-item" href="{{ route('login') }}"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;
                        Đăng nhập</a></li>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a class="dropdown-item" href="{{ route('register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;
                        Tạo tài khoản</a></li>
                @endif
            </ul>
        </div>
    </div>

    <div class="form-group">
        <form method="get" action="{{$url}}">
            <input type="text" name="keyword" class="form-control" placeholder="Search..." value="{{ request()->get('keyword') }}">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</div>