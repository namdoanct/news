<form method="get" action="{{route('category-list-post',  request()->route('id'))}}">
    <div class="form-group">
        <input type="text" name="keyword" class="form-control" value="{{ request()->get('keyword') }}">
    </div>
    <button type="submit" class="btn btn-primary">Search</button>
</form>