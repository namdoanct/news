@extends ('layouts.master')

{{-- set page title --}}
@section('title', __('Danh sách category'))

{{-- import file css (private) --}}
@push('css')
<link rel="stylesheet" href="/css/categories/category-create.css">
@endpush

@section('content')
<form action="{{ route('categories.store') }}" method="post">
    @csrf
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="" class="required">{{ __('Tên category') }}</label>
                <input type="text" name="category_name" class="form-control"> @error('category_name')
                <div class="text-danger">{{ $message }}</div>
                 @enderror
            </div>
        </div>
    </div>
    <p><span class="red">(*)</span>: {{ __('message.required') }}</p>
</form>
@endsection