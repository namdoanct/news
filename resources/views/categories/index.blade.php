@extends ('layouts.master')

{{--set page title --}}
@section('title', __('message.category_list'))

{{-- import file css (private) --}}
@push('css')
<link rel="stylesheet" href="/css/categories/category-list.css">
@endpush

@section('content')

@include('categories.search')
<div class="d-flex flex-wrap">
    @foreach ($posts as $post)
    <div class="w-25">
        <h2>Bài viết</h2>
        <p><a href="{{route('posts.show',$post->id)}}"> {{ $post->name }} </a></p>
        <h2>Danh mục</h2>
        <p> {{ $post->category->name}} </p>
        <h2>Hình ảnh</h2>
        <img src="{{$post->thumbnail}}" alt="{{ $post->name }}">
    </div>
    <hr>
    @endforeach
</div>

<div>
    {{ $posts->appends(request()->input())->links() }}
</div>

@endsection