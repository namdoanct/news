@extends('layouts.master')

{{-- set page title --}}
@section('title', __('message.edit_category'))

{{-- import file css (private) --}}
@push('css')
<link rel="stylesheet" href="/css/categories/category-edit.css">
@endpush

@section('content')
<form action=" {{ route('category.update', $category->id }}" method="post">
    @csrf
    @method('PUT')

    <div class="form-group mb-3">
        <div class="row">
            <div class="col-md-4">
                <label for="" class="required"> {{ __('message.category_name') }}</label>
                <input type="text" name="category_name" value="{{ old('category_name', $category->name }}" class="form-control">
                @error('category_name')
                <div class="text-danger">{{ $message}}</div>
                @enderror
            </div>
        </div>
    </div>
    
    <p><span class="red">(*)</span> {{ __('message.required')}}</p>

    <div class="form-group">
    </div>
</form>
@endsection