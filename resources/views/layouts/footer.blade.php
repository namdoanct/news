 <div class="footer-clean">
     <footer>
         <div class="container">
             <div class="row justify-content-center">
                 <div class="col-sm-4 col-md-3 item">
                     <h3>Dịch vụ</h3>
                     <ul>
                         <li><a href="#">Web design</a></li>
                         <li><a href="#">Development</a></li>
                         <li><a href="#">Hosting</a></li>
                     </ul>
                 </div>
                 <div class="col-sm-4 col-md-3 item">
                     <h3>About</h3>
                     <ul>
                         <li><a href="#">Company</a></li>
                         <li><a href="#">Team</a></li>
                         <li><a href="#">Legacy</a></li>
                     </ul>
                 </div>
                 <div class="col-sm-4 col-md-3 item">
                     <h3>Careers</h3>
                     <ul>
                         <li><a href="#">Job openings</a></li>
                         <li><a href="#">Employee success</a></li>
                         <li><a href="#">Benefits</a></li>
                     </ul>
                 </div>
                 <div class="col-lg-3 item social">
                     <a href="#"><i class="fa fa-facebook-f"></i></a>
                     <a href="#"><i class="icon ion-social-twitter"></i></a>
                     <a href="#"><i class="icon ion-social-snapchat"></i></a>
                     <a href="#"><i class="icon ion-social-instagram"></i></a>
                     <p class="copyright">News © 2023</p>
                 </div>
             </div>
         </div>
     </footer>
 </div>


 <section id="lab_social_icon_footer">
<!-- Include Font Awesome Stylesheet in Header -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
        <div class="text-center center-block">
                <a href="#"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
	            <a href="#"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
	            <a href="#"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
	            <a href="mailto:#"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
    </div>
</div>
</section>