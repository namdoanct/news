<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title', _('Bài viết')) </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{-- css --}}
    @include('layouts.css')
</head>

<body>
    {{-- header --}}
    @include('layouts.header')

    {{-- menu --}}
    @include('layouts.menu')

    {{-- content --}}
    <main>
        <div class="container">

            @yield('content')

        </div>

    </main>

    @include('layouts.subscribe_post')

    {{-- footer --}}
    @include('layouts.footer')
</body>



{{-- js --}}
@include('layouts.js')
<!-- You, 2 weeks ago • add route group, layout master -->
</body>

</html>