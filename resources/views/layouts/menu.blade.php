<div class="container">
   <nav id="menu">
      <ul>
         <li><a href="/">Trang chủ</a></li>
         @foreach($shareCategories as $shareCategory)
         <li><a href="{{route('posts.index', ['category_id'=>$shareCategory->id])}}">{{$shareCategory->name}}</a></li>
         @endforeach
      </ul>
   </nav>
</div>