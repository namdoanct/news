<div class="container text-center">
    <div class="w-50">
    
        <form method="post" action="{{route('subscribe_post')}}" id="form-subscribe">
        @csrf
            <input name="subscribe_email" type="email">
            <button type="submit">Submit</button>
        </form>
    </div>
</div>

// get va post khac nhau the nao
// get: lay du lieu tu server ve
// post: gui du lieu len server

// get: du lieu duoc gui qua url
// post: du lieu duoc gui qua body

// get: du lieu gui di co gioi han
// post: du lieu gui di khong gioi han

// get: du lieu gui di co the bi luu vao lich su trinh duyet
// post: du lieu gui di khong bi luu vao lich su trinh duyet

// get: du lieu gui di co the duoc bookmark
// post: du lieu gui di khong the duoc bookmark

// get: du lieu gui di co the duoc cache
// post: du lieu gui di khong the duoc cache


