@extends('layouts.master')

{{-- css --}}
@include('layouts.css')

@section('content')

<div class="row">
    <div class="col-8">
        <div class="post-detail1">
            <p class="post-name-detail"> {{ $post->name }} </p>
                <div class="">
                    <div class="post-detail-right">
                        <p class="post-category-detail"> <a href="{{route('posts.index', ['category_id'=>$post->category_id])}}">{{ $post->category->name}}</a></p>
                        <p class="post-content-detail">{{ $post->content }}</p>
                    </div>
                </div>
                <div class="">
                    <div class="post-home-thumbnail-detail"><a href="{{route('posts.show',$post->id)}}"> <img src="{{$post->thumbnail}}" alt="{{ $post->name }}" > </a></div>
                </div>
        </div>
    </div>
    <div class="col-4">
        <div class="sidebar">
            <div class="wrapper-sidebar">
                <div class="widget widget-tutorial-nav widget-tutorial">
                    <div class="widget-head">
                        <div class="title">
                            <h2><a href="#"><span class="mdi mdi-chart-timeline"></span>Chủ đề hot</a></h2>
                        </div>
                    </div>
                    <div class="widget-body">
                        <div class="nav-category">
                            <ul class="nav navbar-nav nch-body">
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>
                                <li><a href="#"><i class='fas fa-arrow-alt-circle-right'></i> Gội đầu dưỡng sinh khánh hòa</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection

