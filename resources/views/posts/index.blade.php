@extends('layouts.master')

{{-- css --}}
@include('layouts.css')

@section('content')

<!-- <div class="d-flex flex-wrap"> -->

@php
$count=0;
@endphp
@foreach ($posts as $key=>$post)
@if($key%2==0)<div class="row mb-3 post-all-detail">@endif
    <div class="col-6">
        <div class="row">
            <div class="col-5">
                <div class="postall-home-thumbnail"><a href="{{route('posts.show',$post->id)}}"> <img src="{{$post->thumbnail}}" alt="{{ $post->name }}" class="img-fuild w-75"> </a></div>
            </div>
            <div class="col-7">
                <h5 class="postall-name"><a href="{{route('posts.show',$post->id)}}"> {{ $post->name }} </a></h5>
                <!-- <p><a href="{{route('category-list',$post->category_id)}}"> {{ $post->category->name}} </a></p> -->
                <p class="postall-content"><a href="{{route('posts.show',$post->id)}}"> {{ $post->content }} </a> </p>
            </div>
        </div>
    </div>
    @if($key%2==1)
</div>@endif

@php
$count++;
@endphp
@endforeach
<!-- </div> -->

<div>
    {{ $posts->appends(request()->input())->links() }}
</div>

@endsection