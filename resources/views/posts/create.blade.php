@extends('layouts.master')

@section('content')


<form>
    <div class="form-group">
        <label>Post name</label>
        <input type="text" class="form-control"  placeholder="postname">
    </div>

    <div class="form-group">
        <label>Post thumbnail</label>
        <input type="file" class="form-control"  placeholder="postname">
    </div>

    <div class="form-group">
        <label>Category</label>
        <select class="form-control">
            <option>Chon category</option>
        @foreach ($categories as $categoryId => $categoryName)
            <option value="{{$categoryId}}">{{$categoryName}}</option>
        @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>Post content</label>
        <textarea class="form-control" rows="3"></textarea>
    </div>
</form>






@endsection