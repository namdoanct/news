<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>title</title>
    <link rel="stylesheet" href="linkToCSS" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/login.css">
</head>

<body>
    <form method="post" action="{{route('login.handle')}}" class="form-login">
        @csrf
        <h2>
            Đăng Nhập
        </h2>
        <input type="email" name="email" value="{{old('email')}}" placeholder="Nhập email">
        @error('email')
        <div class="text-danger">{{ $message }}</div>
        @enderror

        <input type="password" name="password" placeholder="Nhập password">
        @error('password')
        <div class="text-danger">{{ $message }}</div>
        @enderror
        <label>
        <input type="checkbox" name="remember" value="1" placeholder="Remember"> Remember
        </label>
        

        <button type="submit">Login</button>
    </form>
</body>

</html>







