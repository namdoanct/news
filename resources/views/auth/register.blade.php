<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>title</title>
    <link rel="stylesheet" href="linkToCSS" />
    <link rel="stylesheet" href="/css/register.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <form class="form-register" method="POST" action="{{ route('register') }}">
        @csrf
        <h2>Đăng ký</h2>
        <!-- Name -->
        <div>
            <input id="name" class="block mt-1 w-full" placeholder="Name" type="text" name="name" value="{{ old('name') }}"  autofocus />
            @error('name')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <!-- Email Address -->
        <div class="mt-4">
            <input id="email" class="block mt-1 w-full" placeholder="Email" type="email" name="email" value="{{ old('email') }}"  />
            @error('email')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <!-- Password -->
        <div class="mt-4">
            <input id="password" class="block mt-1 w-full" placeholder="Password" type="password" name="password"  autocomplete="new-password" />
            @error('password')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <input id="password_confirmation" class="block mt-1 w-full" placeholder="Confirm Password" type="confirm-password" name="password_confirmation"  />
            @error('password_confirmation')
            <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="flex items-center justify-end mt-4">
            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                {{ __('Already registered?') }}
            </a>

            <button class="ml-4" type="submit">
                {{ __('Register') }}
            </button>

        </div>
    </form>
</body>

</html>