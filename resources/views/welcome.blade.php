@extends('layouts.master')

{{-- set page title --}}
@section('title', 'Home Page')

{{-- css --}}
@include('layouts.css')




@section('content')

<!-- @include('home_parts.advertisement') -->



<div class="row">
    <div class="col-md-3" >@include('home_parts.post_feature')</div>
    <div class="col-md-9" >@include('home_parts.posts_list')</div>
</div>






<!-- @include('home_parts.slide') -->

@endsection



<!-- import cac file javascript chi dung rieng cho homepage -->
@push('js')

@endpush

<!-- import cac file css chi dung rieng cho homepage -->
@push('css')
<link rel="stylesheet" href="/css/home.css">
@endpush